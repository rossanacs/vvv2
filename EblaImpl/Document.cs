/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
using MySql.Data.MySqlClient;
using System.Xml;

namespace EblaImpl
{
    internal class Document : OpenableEblaItem, IDocument
    {
        private int _corpusid;
        private int _id;
        private string _name;
        private string _corpusname;
        private DocumentMetadata _meta = new DocumentMetadata();

        XmlDocument _content; // TODO - cache in singleton.

        int _length = 0;

        XmlDocument _TOC;



        public Document(System.Collections.Specialized.NameValueCollection configProvider)
            : base(configProvider)
        {
        }


        private bool Open(string CorpusName, string Username, string Password, string NameIfVersion)
        {
            if (!base.Open(Username, Password))
                return false;

            OpenWithoutPrivilegeCheck(CorpusName, NameIfVersion, false, null);
            CheckCanRead(_corpusname);
            return true;
        }

        internal void OpenWithoutPrivilegeCheck(string CorpusName, string NameIfVersion, bool makeAdmin, MySqlConnection cn)
        {
            if (makeAdmin)
                base.MakeAdmin();

            if (cn != null)
                SetConnection(cn);

            using (MySqlCommand cmd = new MySqlCommand("SELECT ID FROM corpora WHERE Name = @name", GetConnection()))
            {
                MySqlParameter nameParm = cmd.Parameters.AddWithValue("name", CorpusName);
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception("Corpus not found: " + CorpusName);

                    _corpusid = dr.GetInt32(dr.GetOrdinal("ID"));

                }

                //_isBaseText = true;
                cmd.CommandText = "SELECT ID, Content, Length, TOC, Description, Information, ReferenceDate, AuthorTranslator, Genre, CopyrightInfo, LanguageCode FROM documents WHERE CorpusID = " + _corpusid.ToString() + " AND ";
                string notFoundMsg = "Corpus base text text not found: " + CorpusName;
                if (string.IsNullOrEmpty(NameIfVersion))
                {
                    cmd.CommandText += "Name IS NULL";
                }
                else
                {
                    cmd.CommandText += "Name = @name";
                    nameParm.Value = NameIfVersion;
                    //_isBaseText = false;
                    notFoundMsg = "Version text not found: " + NameIfVersion;
                }

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception(notFoundMsg);

                    _id = dr.GetInt32(dr.GetOrdinal("ID"));

                    int contentOrdinal = dr.GetOrdinal("Content");
                    int tocOrdinal = dr.GetOrdinal("TOC");
                    int lengthOrdinal = dr.GetOrdinal("Length");

                    if (!dr.IsDBNull(lengthOrdinal))
                        _length = dr.GetInt32(lengthOrdinal);

                    if (dr.IsDBNull(contentOrdinal))
                    {

                    }
                    else
                    {
                        string content = dr.GetString(contentOrdinal);
                        if (content.Length > 0)
                        {
                            _content = new XmlDocument();
#if _VVV_PRESERVEWHITESPACE
                            _content.PreserveWhitespace = true;
#endif
                            _content.LoadXml(content);
#if _VVV_PRESERVEWHITESPACE
                            EblaHelpers.ConvertWhitespaceNodesToTextNodes(_content.DocumentElement);
#endif
                        }
                    }
                    if (dr.IsDBNull(tocOrdinal))
                    {

                    }
                    else
                    {
                        string toc = dr.GetString(tocOrdinal);
                        if (toc.Length > 0)
                        {
                            _TOC = new XmlDocument();
                            _TOC.LoadXml(toc);
                        }
                    }

                    int descOrdinal = dr.GetOrdinal("Description");
                    if (!dr.IsDBNull(descOrdinal))
                        _meta.Description = dr.GetString(descOrdinal);
                    
                    int infoOrdinal = dr.GetOrdinal("Information");
                    if (!dr.IsDBNull(infoOrdinal))
                        _meta.Information = dr.GetString(infoOrdinal);

                    int refdateOrdinal = dr.GetOrdinal("ReferenceDate");
                    if (!dr.IsDBNull(refdateOrdinal))
                        _meta.ReferenceDate = dr.GetInt32(refdateOrdinal);

                    int authtransOrdinal = dr.GetOrdinal("AuthorTranslator");
                    if (!dr.IsDBNull(authtransOrdinal))
                        _meta.AuthorTranslator = dr.GetString(authtransOrdinal);

                    int genreOrdinal = dr.GetOrdinal("Genre");
                    if (!dr.IsDBNull(genreOrdinal))
                        _meta.Genre = dr.GetString(genreOrdinal);

                    int copyrightOrdinal = dr.GetOrdinal("CopyrightInfo");
                    if (!dr.IsDBNull(copyrightOrdinal))
                        _meta.CopyrightInfo = dr.GetString(copyrightOrdinal);

                    int langcodeOrdinal = dr.GetOrdinal("LanguageCode");
                    if (!dr.IsDBNull(langcodeOrdinal))
                        _meta.LanguageCode = dr.GetString(langcodeOrdinal);

                }

            }

            _open = true;
            _name = NameIfVersion;
            _corpusname = CorpusName;

            
        }


        public bool OpenBaseText(string CorpusName, string Username, string Password)
        {
            return Open(CorpusName, Username, Password, null);

        }

        public bool OpenVersion(string CorpusName, string VersionName, string Username, string Password)
        {
            return Open(CorpusName, Username, Password, VersionName);
        }


        public DocumentMetadata GetMetadata()
        {
            return _meta;
        }

        public long GetSegmentCount()
        {
            string sql = "SELECT count(*) FROM segmentdefinitions WHERE DocumentID = " + _id.ToString();

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                object o = cmd.ExecuteScalar();

                if (o != null)
                    if (o != DBNull.Value)
                        return (long)o;
            }
            return 0;
        }

        public void SetMetadata(DocumentMetadata meta)
        {
            using (MySqlCommand cmd = new MySqlCommand("UPDATE documents SET Description = @description, Information = @information, ReferenceDate = @referencedate, AuthorTranslator = @authortranslator, Genre = @genre, CopyrightInfo = @copyrightinfo, LanguageCode = @langcode WHERE ID = " + _id.ToString(), GetConnection()))
            {
                cmd.Parameters.AddWithValue("description", meta.Description);
                cmd.Parameters.AddWithValue("information", meta.Information);
                cmd.Parameters.AddWithValue("authortranslator", meta.AuthorTranslator);
                cmd.Parameters.AddWithValue("genre", meta.Genre);
                cmd.Parameters.AddWithValue("copyrightinfo", meta.CopyrightInfo);
                if (meta.ReferenceDate.HasValue)
                    cmd.Parameters.AddWithValue("referencedate", meta.ReferenceDate.Value);
                else
                    cmd.Parameters.AddWithValue("referencedate", DBNull.Value);
                cmd.Parameters.AddWithValue("langcode", meta.LanguageCode);

                cmd.ExecuteNonQuery();
            }
            
        }

        public int Length()
        {
            if (_content == null)
                return 0;

            return _length;
        }


        public string GetDocumentContentText(int StartPosition, int Length)
        {
            CheckOpen();
            CheckNotEmpty();
            CheckCanRead(_corpusname);

            CheckPosValues(StartPosition, Length);

            if (Length == 0)
                return string.Empty;

            List<XmlElement> elmsToIgnore = new List<XmlElement>();

            int i = StartPosition;
            XmlDocPos startPos = XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);


            i = StartPosition + Length;
            XmlDocPos endPos = XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);

            XmlDocument resultsDoc = EblaHelpers.CreateXmlDocument("content");

            CloneStatus status = new CloneStatus();
            status.StartPosFound = false;
            status.LengthRemaining = Length;

            // This is a rather expensive way of getting just the text content between two positions.
            // TODO - optimise.
            CloneBetweenPositions(_content.DocumentElement, resultsDoc.DocumentElement, startPos, status, false, null);

            //return resultsDoc.DocumentElement.InnerText;
            // Can't do that, as we end up getting words joined together.
            return System.Text.RegularExpressions.Regex.Replace(resultsDoc.DocumentElement.InnerXml, "<.*?>", " ");
            // This means we may end up with multiple spaces, but let's not worry about that given what this
            // function is meant for.
        }

        static public bool IgnoreElm(XmlNode node)
        {
            if (node.NodeType != XmlNodeType.Element)
                return false;

            XmlElement elm = (XmlElement)node;

            string eblatype = elm.GetAttribute("data-eblatype");
            if (string.IsNullOrEmpty(eblatype))
                return false;
            switch (eblatype)
            {
                case "startmarker":
                case "endmarker":
                    return true;
            }

            return false;
        }

        private bool IsInternalAttrib(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                if (name[0] == ':')
                    return true;
            }

            return false;

        }

        public string GetDocumentContent(int StartPosition, int Length, bool addSegmentMarkers, bool addSegmentFormatting, SegmentAttribute[] AttributeFilters, bool forEdit)
        {
            bool addIds = false;

            if (forEdit)
            {
                if (!addSegmentMarkers)
                    throw new Exception("GetDocumentContent: addSegmentMarkers must be true if forEdit is true");
                if (addSegmentFormatting)
                    throw new Exception("GetDocumentContent: addSegmentFormatting must not be true if forEdit is true");
            }

            CheckOpen();
            CheckNotEmpty();
            CheckCanRead(_corpusname);

            CheckPosValues(StartPosition, Length);

            if (Length == 0)
                return string.Empty;

            //List<XmlElement> elmsToIgnore = new List<XmlElement>();

            int i = StartPosition;
            XmlDocPos startPos =  XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);

            if (startPos == null)
                return string.Empty;

            i = StartPosition + Length;
            XmlDocPos endPos =  XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);

           

            XmlDocument resultsDoc = EblaHelpers.CreateXmlDocument("content");

            CloneStatus status = new CloneStatus();
            status.StartPosFound = false;
            status.LengthRemaining = Length;
            
            CloneBetweenPositions(_content.DocumentElement, resultsDoc.DocumentElement, startPos, status, addIds, null);

            List<XmlAttribute> segformatAttribs = new List<XmlAttribute>();
            List<XmlAttribute> startmarkerAttribs = new List<XmlAttribute>();
            List<XmlAttribute> endmarkerAttribs = new List<XmlAttribute>();
            List<XmlAttribute> userAttribs = new List<XmlAttribute>(); 
            XmlAttribute segformatAttrib = resultsDoc.CreateAttribute("data-eblatype");
            segformatAttrib.Value = "segformat";
            XmlAttribute startmarkerAttrib = resultsDoc.CreateAttribute("data-eblatype");
            startmarkerAttrib.Value = "startmarker";
            XmlAttribute endmarkerAttrib = resultsDoc.CreateAttribute("data-eblatype");
            endmarkerAttrib.Value = "endmarker";

            List<SegmentDefinition> segDefs = new List<SegmentDefinition>();
            if (addSegmentFormatting || addSegmentMarkers)
            {
                segDefs.AddRange(FindSegmentDefinitions(StartPosition, Length, true, true, AttributeFilters, true));

            }


            if (addSegmentFormatting)
            {
                

                int lastNonTextNodeOffset = 0;
                XmlNode lastNonTextNode = null;

                // Get segment defs arranged in a tree that represents hierarchical enclosure
                // (pass 'true' to get a flat list of all nodes, whose 'children' still represent enclosure
                List<SegmentTreeNode> flatTree = SegmentTreeNode.SegmentsToTree(segDefs, true);

                foreach (var node in flatTree)
                {
                    // If segment formatting for each segment were applied simply by placing a start and end tag around 
                    // the entire segment, life would be simple. But because segments can start and end anywhere in the 
                    // HTML document, trying to do that could make the document invalid (e.g. our tag pair could enclose 
                    // a div start tag without enclosing its end tag). So, our 'greedy enclosure' algorithm tries to enclose all
                    // segment content within tag pairs, using as few pairs as possible but ensuring the content remains valid. 
                    // For example, given this HTML:
                    //  <body>
                    //  The quick brown fox jumps over the lazy dog.
                    //  <div>
                    //  Sphinx of black quartz, judge my vow.
                    //  </div>
                    //  </body>
                    // If the segment definition starts with the word 'fox' and ends with the word 'black',
                    // the valid enclosure produced will be something like this:
                    //  <body>
                    //  The quick brown <seg>fox jumps over the lazy dog.</seg>
                    //  <div>
                    //  <seg>Sphinx of black</seg> quartz, judge my vow.
                    //  </div>
                    //  </body>
                    // The algorithm is 'greedy' because rather than enclosing every text node in a separate span,
                    // it attempts to enclose multiple elements (inline or block) with a single tag pair wherever possible.

                    // Given that this approach is required, a further complication arises when one segment contains another.
                    // For example, suppose we have a segment S1 with start offset 10 and length 30, and a segment S2 with start
                    // offset 20 and length 10. The range covered by S1 includes the range covered by S2. If segments were 
                    // formatting simply by adding a single enclosing tag pair for each segment, then it would be clear when 
                    // parsing the document which text was in S2 and which in S2 - and, indeed, the tag for S2 could be
                    // given a different CSS class and would be rendered differently from S1, could be given different 'hover'
                    // text, et.
                    // However, because of the requirement to use multiple tag pairs as shown above, applying greedy enclosure
                    // to S1 and S2 as-is means that some segments of text may be enclosed by (say) span tags with seemingly
                    // conflicting attributes, making it harder to identify segment membership, render segments distinctly, etc.
                    // So, before applying segment formatting, we arrange the segment definitions as a tree that expresses
                    // any segment containment. If a segment contains no 'child' segments, greedy enclosure is applied to its
                    // content as-is. If a segment does contain 'child' segments, greedy enclosure is applied only to the 
                    // sections of the segment that are not also part of a 'child' segment.


                    // For this node's SegmentDefinition, find out how many enclosure stretches are needed.
                    List<DocumentRange> enclosures = node.SubtractDescendants();

                    // Set up attributes
                    XmlAttribute eblasegidAttrib = resultsDoc.CreateAttribute("data-eblasegid");
                    eblasegidAttrib.Value = node.SegmentDefinition.ID.ToString();
                    userAttribs.Clear();
                    if (node.SegmentDefinition.Attributes != null)
                    {
                        XmlAttribute titleAttrib = resultsDoc.CreateAttribute("title");
                        string title = string.Empty;
                        foreach (var a in node.SegmentDefinition.Attributes)
                        {
                            string attribName = a.Name;
                            string prefix = "data-userattrib-";
                            bool internalAttrib = IsInternalAttrib(a.Name);
                            if (internalAttrib)
                            {
                                attribName = attribName.Substring(1);
                                prefix = "data-ebla-";

                            }

                            XmlAttribute userAttrib = resultsDoc.CreateAttribute(prefix + attribName);
                            userAttrib.Value = a.Value;
                            userAttribs.Add(userAttrib);
                            // Exclude attribs beginning with ':' ... we 'reserve' that for internal use (e.g. viv/eddy)
                            if (!internalAttrib)
                            {

                                if (title.Length > 0)
                                    title += ", ";
                                title += a.Name + ": " + a.Value;
                            }
                        }
                        titleAttrib.Value = title;
                        userAttribs.Add(titleAttrib);
                    }

                    segformatAttribs.Clear();
                    segformatAttribs.Add(segformatAttrib);
                    //segformatAttribs.Add(segformatClass);
                    segformatAttribs.Add(eblasegidAttrib);
                    segformatAttribs.AddRange(userAttribs);

                    // add a greedy enclosure for each
                    foreach (var e in enclosures)
                    {
#if DEBUG
                        //if (e.Length == 0)
                        //{
                        //    string s = "debug";

                        //}
#endif

                        // although the segment as a whole must overlap our content section somewhere, 
                        // an individual enclosure (after child subtraction) might not.
                        if ((e.StartPosition + e.Length) <= StartPosition)
                            continue;
                        if (e.StartPosition >= (StartPosition + Length))
                            continue;

                        i = e.StartPosition - StartPosition;
                        if (i < 0) i = 0;
                        startPos = XmlDocPos.SeekToOffsetEx(i, resultsDoc.DocumentElement, IgnoreElm, ref lastNonTextNode, ref lastNonTextNodeOffset);

                        if (startPos == null)
                        {
                            // TODO - log
                            throw new Exception("Unable to seek to start offset " + i.ToString() + " while adding segment enclosure markup");
                        }

                        i = e.StartPosition + e.Length - StartPosition;
                        if (i < 0) i = 0;
                        if (i > Length) i = Length;
                        endPos = XmlDocPos.SeekToOffsetEx(i, resultsDoc.DocumentElement, IgnoreElm, ref lastNonTextNode, ref lastNonTextNodeOffset);

                        if (endPos == null)
                        {
                            // TODO - log
                            throw new Exception("Unable to seek to end offset " + i.ToString() + " while adding segment enclosure markup");
                        }

                        // If we're right at the end of a text node, try to insert it at the
                        // start of the next instead.
                        // (this also assists the 'promotion' optimisation)
                        if (startPos.offset == startPos.textContent.Length)
                        {
                            XmlText nextText = NextTextNode(startPos.textContent.NextSibling, startPos.textContent.ParentNode, IgnoreElm);
                            if (nextText != null)
                            {
                                startPos.textContent = nextText;
                                startPos.offset = 0;
                            }
                        }

                        // Try 'promotion' optimisation
                        bool changesMade = false;
                        XmlNode startNode = startPos.textContent;
                        XmlNode endNode = endPos.textContent;
                        int startOffset = startPos.offset;
                        int endOffset = endPos.offset;
                        bool noTextBeforeSeg = startPos.offset == 0;
                        bool noTextAfterSeg = endPos.offset == endPos.textContent.Value.Length;

                        do
                        {
                            changesMade = false;

                            // if the start pos was right at the beginning of the text elm, and if the current
                            // start node is the first child of its parent ...
                            if (noTextBeforeSeg)
                                if (startNode.ParentNode != null)
                                    if (object.ReferenceEquals(startNode.ParentNode.ChildNodes[0], startNode))
                                    {
                                        // It may be worth 'promoting' the start node.

                                        // We can promote the start node to the parent so long as the
                                        // parent is not an ancestor of the end node (because if it is,
                                        // we shouldn't enclose the entire parent)
                                        if (!IsAncestor(endNode, startNode.ParentNode))
                                        {
                                            startNode = startNode.ParentNode;
                                            startOffset = -1; // tell enclosure algorithm not to worry about it
                                            changesMade = true;
                                            continue;
                                        }
                                    }


                            if (noTextAfterSeg)
                                if (endNode.ParentNode != null)
                                    if (object.ReferenceEquals(endNode.ParentNode.ChildNodes[endNode.ParentNode.ChildNodes.Count - 1], endNode))
                                    {
                                        if (!IsAncestor(startNode, endNode.ParentNode))
                                        {
                                            endNode = endNode.ParentNode;
                                            endOffset = -1; // tell enclosure algorithm not to worry about it
                                            changesMade = true;
                                            continue;
                                        }


                                    }

                            if (noTextBeforeSeg && noTextAfterSeg)
                                if (startNode.ParentNode != null && endNode.ParentNode != null)
                                    if (object.ReferenceEquals(startNode.ParentNode, endNode.ParentNode))
                                    {

                                    }
                        }
                        while (changesMade);


                        bool endFound = GreedyEnclosure(startNode, startOffset, endNode, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);

                        System.Diagnostics.Debug.Assert(endFound);

                    }
                }
                
                

            }

            if (addSegmentMarkers)
            {

                int lastNonTextNodeOffset = 0;
                XmlNode lastNonTextNode = null;

                foreach (SegmentDefinition segdef in segDefs)
                {
                    XmlAttribute eblasegidAttrib = resultsDoc.CreateAttribute("data-eblasegid");
                    eblasegidAttrib.Value = segdef.ID.ToString();
                    userAttribs.Clear();
                    if (!forEdit)
                        if (segdef.Attributes != null)
                            foreach (var a in segdef.Attributes)
                            {
                                string attribName = a.Name;
                                string prefix = "data-userattrib-";
                                bool internalAttrib = IsInternalAttrib(a.Name);
                                if (internalAttrib)
                                {
                                    attribName = attribName.Substring(1);
                                    prefix = "data-ebla-";

                                }

                                XmlAttribute userAttrib = resultsDoc.CreateAttribute(prefix + attribName);
                                userAttrib.Value = a.Value;
                                userAttribs.Add(userAttrib);
                            }


                    endmarkerAttribs.Clear();
                    endmarkerAttribs.Add(endmarkerAttrib);
                    endmarkerAttribs.Add(eblasegidAttrib);
                    endmarkerAttribs.AddRange(userAttribs);

                    startmarkerAttribs.Clear();
                    startmarkerAttribs.Add(startmarkerAttrib);
                    startmarkerAttribs.Add(eblasegidAttrib);
                    startmarkerAttribs.AddRange(userAttribs);

                    if (!forEdit)
                    {
                        XmlAttribute eblahtmlidAttrib = resultsDoc.CreateAttribute("id");
                        eblahtmlidAttrib.Value = "ebla-segstart-" + segdef.ID.ToString();
                        startmarkerAttribs.Add(eblahtmlidAttrib);
                        eblahtmlidAttrib = resultsDoc.CreateAttribute("id");
                        eblahtmlidAttrib.Value = "ebla-segend-" + segdef.ID.ToString();
                        endmarkerAttribs.Add(eblahtmlidAttrib);
                    }

                    int startOffset = segdef.StartPosition - StartPosition;

                    bool showStartSegElm = false;
                    bool showEndSegElm = false;
                    if (startOffset >= 0 && startOffset <= Length)
                        showStartSegElm = true;

                    int endOffset = segdef.StartPosition + segdef.Length - StartPosition;
                    if (endOffset >= 0 && endOffset <= Length)
                        showEndSegElm = true;

                    if (showStartSegElm && showEndSegElm) /// only ever show both
                    {
                        XmlDocPos pos = XmlDocPos.SeekToOffsetEx(startOffset, resultsDoc.DocumentElement, IgnoreElm, ref lastNonTextNode, ref lastNonTextNodeOffset);


                        if (pos == null)
                        {
                            // TODO - log
                            throw new Exception("Unable to seek to start offset " + i.ToString() + " while adding segment markers");
                        }

                        // If we're right at the end of a text node, try to insert it at the
                        // start of the next instead.
                        if (pos.offset == pos.textContent.Length)
                        {
                            XmlText nextText = NextTextNode(pos.textContent.NextSibling, pos.textContent.ParentNode, IgnoreElm);                            if (nextText != null)
                            {
                                pos.textContent = nextText;
                                pos.offset = 0;
                            }
                        }

                        //elmsToIgnore.Add(InsertStartSegElm(pos, resultsDoc, startmarkerAttribs));
                        InsertStartSegElm(pos, resultsDoc, startmarkerAttribs, forEdit);

                        pos = XmlDocPos.SeekToOffsetEx(endOffset, resultsDoc.DocumentElement, IgnoreElm, ref lastNonTextNode, ref lastNonTextNodeOffset);

                        if (pos == null)
                        {
                            // TODO - log
                            throw new Exception("Unable to seek to end offset " + i.ToString() + " while adding segment markers");
                        }

                        System.Diagnostics.Debug.Assert(pos != null);

                        //elmsToIgnore.Add(InsertEndSegElm(pos, resultsDoc, endmarkerAttribs));
                        InsertEndSegElm(pos, resultsDoc, endmarkerAttribs, forEdit);

                    }

                }
            }



            return resultsDoc.DocumentElement.InnerXml; 


        }

        private bool GreedyEnclosure(XmlNode startNode, int startOffset, XmlNode endNode, int endOffset, List<XmlAttribute> segformatAttribs/*, bool addStartMarker, bool addEndMarker, List<XmlAttribute> startmarkerAttribs, List<XmlAttribute> endmarkerAttribs, List<XmlElement> elmsToIgnore*/)
        {

            // Traverse forward through siblings from startNode, looking at nodes found and checking whether endPos
            // hit. Group nodes found into those that can be enclosed in a span (inline), and those that need
            // a div (block-level). If a node is found that is an ancestor of endPos, apply span/div enclosure 
            // to whatever preceded, then recurse down into that node. If enclosure is applied to all siblings 
            // and no ancester of endPos is found, recurse up through parent siblings. If endPos itself is
            // found (necessarily while grouping inline nodes), enclose in span and quit.

            // Certain elements can be 
            // either block-level or inline. We're treating most of those as inline (see inline tag list), which
            // presumes they will not then be used to contain block-level elements. If they are, then the html
            // we emit will be invalid, so this code may need to be enhanced.


            XmlDocument doc = startNode.OwnerDocument;

            List<XmlNode> nodesForDiv = new List<XmlNode>();
            List<XmlNode> nodesForSpan = new List<XmlNode>();

            if (startOffset >= 0)
            {
                System.Diagnostics.Debug.Assert(startNode != null);
                System.Diagnostics.Debug.Assert(startNode.NodeType == XmlNodeType.Text);

            }
            else
            {
                System.Diagnostics.Debug.Assert(startOffset == -1); // value meaning 'ignore'
                // set it to a no-effect value
                startOffset = 0;
            }
            if (endOffset >= 0)
            {
                System.Diagnostics.Debug.Assert(endNode != null);
                System.Diagnostics.Debug.Assert(endNode.NodeType == XmlNodeType.Text);

            }
            else
            {
                System.Diagnostics.Debug.Assert(endOffset == -1); // value meaning 'ignore'
            }
            if (startNode.NodeType == XmlNodeType.Text)
            {
                
                // Check for trivial case
                if (object.ReferenceEquals(startNode, endNode))
                {
                    nodesForSpan.Add(startNode);
                    EncloseInSpan(nodesForSpan, startOffset, startNode.OwnerDocument, startNode.ParentNode, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
                    return true;
                }
            }

            XmlNode parent = startNode.ParentNode;

            bool traversingBlockElms = IsBlockElm(startNode);
            XmlNode currentNode = startNode;
            bool endFound = false;
            int spanLength = 0;

            while (currentNode != null)
            {
                if (IsAncestor(endNode, currentNode))
                {
                    // This enclosure will have to stop at this element.
                    endFound = true;
                    break;
                }

                if (IsBlockElm(currentNode))
                {
                    if (!traversingBlockElms)
                    {
                        // We just transitioned from one or more inline elms (or text nodes)
                        // to a block elm.
                        traversingBlockElms = true;

                        // put a span around everything in nodesForSpan
                        EncloseInSpan(nodesForSpan, startOffset, doc, parent, -1, segformatAttribs/*, addStartMarker, false, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
                        nodesForSpan.Clear();
                        startOffset = 0;

                    }
                    nodesForDiv.Add(currentNode);

                }
                else
                {
                    if (traversingBlockElms)
                    {
                        // We just transitioned from one or more block elms
                        // to an inline elm

                        // put a div around everything in nodesforDiv
                        // and use that div to replace them
                        EncloseInDiv(nodesForDiv, doc, parent, segformatAttribs);
                        nodesForDiv.Clear();

                        traversingBlockElms = false;
                    }
                    nodesForSpan.Add(currentNode);
                    spanLength += currentNode.InnerText.Length;
                }

                if (object.ReferenceEquals(currentNode, endNode))
                {
                    //System.Diagnostics.Debug.Assert(!traversingBlockElms);
                    if (traversingBlockElms)
                    {
                        System.Diagnostics.Debug.Assert(nodesForDiv.Contains(currentNode));
                        EncloseInDiv(nodesForDiv, doc, parent, segformatAttribs);
                    }
                    else
                    {
                        System.Diagnostics.Debug.Assert(nodesForSpan.Contains(currentNode));
                        EncloseInSpan(nodesForSpan, startOffset, doc, parent, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
                    }

                    return true;
                }

                currentNode = currentNode.NextSibling;
                               
            }


            if (nodesForDiv.Count > 0)
                EncloseInDiv(nodesForDiv, doc, parent, segformatAttribs);


            if (nodesForSpan.Count > 0)
            {
                // Don't bother putting in formatting/markers if span would be empty (no text-bearing elements)
                // or startOffset would mean formatting starts after the last char.
                if (spanLength - startOffset > 0)
                {
                    EncloseInSpan(nodesForSpan, startOffset, doc, parent, -1, segformatAttribs/*, addStartMarker, false, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
                    startOffset = 0;

                }


            }

            if (endFound)
            {
                // An enclosure traversal stopped at this element, because it's
                // an ancestor of endPos.
                // Apply greedy enclosure to its children.

                endFound = true;
                if (!GreedyEnclosure(currentNode.FirstChild, -1, endNode, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/))
                    throw new Exception("Failed to find endPos from ancestor node");

                return true;
            }

            if (!endFound)
            {
                // Ok, so we traversed forward through all the siblings of the node provided, and didn't
                // find the end of the segment to enclose. That exhausts everything in the parent node.
                // So, start looking from the parent's next sibling, if there is one.
                currentNode = parent;
                // climb up ancestors until a next sibling is found.
                while (currentNode != null)
                {
                    if (currentNode.NextSibling != null)
                    {
                        currentNode = currentNode.NextSibling;
                        break;
                    }
                    currentNode = currentNode.ParentNode;
                }
                if (currentNode != null)
                    endFound = GreedyEnclosure(currentNode, -1, endNode, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
            }


            return endFound;

        }

        internal static XmlText NextTextNode(XmlNode node, XmlNode parent, XmlDocPos.IgnoreElmDelg IgnoreElmImpl /*, List<XmlElement> elmsToIgnore*/)
        {

            while (node != null)
            {
                if (!IgnoreElmImpl.Invoke(node)) //  !elmsToIgnore.Contains(node))
                {
                    if (node.NodeType == XmlNodeType.Text)
                        return (XmlText)node;

                    if (node.HasChildNodes)
                    {
                        XmlText child = NextTextNode(node.ChildNodes[0], parent, IgnoreElmImpl);
                        if (child != null)
                            return child;
                    }

                }
                node = node.NextSibling;
            }

            if (parent == null)
                return null;

            return NextTextNode(parent.NextSibling, parent.ParentNode, IgnoreElmImpl);
        }

        private XmlNode EncloseInSpan(List<XmlNode> nodesForSpan, int spanStartOffset, XmlDocument doc, XmlNode parent, int spanEndOffset, List<XmlAttribute> segformatAttribs/*, bool addStartMarker, bool addEndMarker, List<XmlAttribute> startmarkerAttribs, List<XmlAttribute> endmarkerAttribs, List<XmlElement> elmsToIgnore*/)
        {
            
            // put a span around everything in nodesForSpan
            XmlElement span = doc.CreateElement("span");
            foreach (XmlAttribute attrib in segformatAttribs)
            {
                XmlAttribute newAttrib = doc.CreateAttribute(attrib.Name);
                newAttrib.Value = attrib.Value;
                span.Attributes.Append(newAttrib);
            }

            System.Diagnostics.Debug.Assert(nodesForSpan.Count > 0);
            XmlNode insertAfterNode = nodesForSpan[0].PreviousSibling;
            XmlNode appendToSpanNode = null;
            XmlNode appendAfterSpanNode = null;

            XmlNode lastNode = nodesForSpan[nodesForSpan.Count - 1];
            int nodesProvidedCount = nodesForSpan.Count;
            bool firstNodeSplit = false;
            if (spanStartOffset > 0)
            {
                System.Diagnostics.Debug.Assert(nodesForSpan[0].NodeType == XmlNodeType.Text);
                XmlText text = (XmlText)(nodesForSpan[0]);
                nodesForSpan.RemoveAt(0);
                insertAfterNode = text;
                System.Diagnostics.Debug.Assert(spanStartOffset <= text.Length);
                if (spanStartOffset == text.Length)
                {

                }
                else
                {
                    // Split text node
                    XmlText newText = doc.CreateTextNode(text.Value.Substring(spanStartOffset));
                    text.Value = text.Value.Substring(0, spanStartOffset);
                    span.AppendChild(newText);
                    //firstNode = newText;
                    firstNodeSplit = true;
                    if (nodesProvidedCount == 1)
                        lastNode = newText;
                }
            }

            if (spanEndOffset > -1)
            {
                System.Diagnostics.Debug.Assert(lastNode.NodeType == XmlNodeType.Text);
                XmlText text = (XmlText)(lastNode);
                if (nodesProvidedCount == 1)
                {
                    
                    // if spanStartOffset > 1, it will have been split, making remaining text shorter.
                    // Adjust end offset.
                    spanEndOffset -= spanStartOffset;
                }
                else
                {
                }

                if (spanEndOffset == 0)
                {
                    // Don't bother including in span
                    if (nodesProvidedCount == 1)
                    {
                        // The node that was originally in nodesForSpan may have been removed
                        // from it, and left as a child of the parent node. This is the case
                        // if 1. start offset was > 0, in which case it will have been split and a new node added to span, or
                        // 2. start offset == length, in which case nothing will have been added to span.

                        if (nodesForSpan.Count == 0)
                        {
                            // It has indeed been removed
                            if (span.ChildNodes.Count == 0)
                            {
                                // case 2 applies - we don't need to do anything
                            }
                            else
                            {

                                // The node that was originally in nodesForSpan has been removed
                                // from it, and left as a child of the parent node. However,
                                // its content has been truncated. The new node created with the
                                // remaining content is only in the span. We need to take it out
                                // of the span, then append it after the (seemingly empty) span

                                span.RemoveChild(lastNode);
                                appendAfterSpanNode = lastNode;
                            }

                        }
                        else
                        {
                            nodesForSpan.Clear();
                        }

                    }
                    else
                        nodesForSpan.RemoveAt(nodesForSpan.Count - 1);
                }
                else
                {
                    // Split text node if needs be
                    if (spanEndOffset < text.Value.Length)
                    {
                        //string s = text.Value;
                        if (nodesProvidedCount == 1 && firstNodeSplit)
                        {
                            // The node that was originally in nodesForSpan has been removed
                            // from it, so will be left as a child of the parent node. However,
                            // its content has been truncated. The new node created with the
                            // remaining content is only in the span. We need to truncate it,
                            // then create another node with the remainder for appending
                            // after the span
                            XmlText newText = doc.CreateTextNode(lastNode.Value.Substring(spanEndOffset));
                            lastNode.Value = lastNode.Value.Substring(0, spanEndOffset);
                            appendAfterSpanNode = newText;
                        }
                        else
                        {

                            // Create a new text node with just the leftmost content before the end offset
                            XmlText newText = doc.CreateTextNode(text.Value.Substring(0, spanEndOffset));
                            // Record it as one we'll append onto the end of the span
                            appendToSpanNode = newText;
                            // Modify the existing node so as only to include the content after the end offset
                            text.Value = text.Value.Substring(spanEndOffset);
                            // Take it out of the list so it won't be removed from the parent
                            nodesForSpan.Remove(text);

                        }

                    }

                }
            }

            foreach (var n in nodesForSpan)
            {
                parent.RemoveChild(n);
                span.AppendChild(n);
            }

            if (appendToSpanNode != null)
                span.AppendChild(appendToSpanNode);

            if (insertAfterNode != null)
                parent.InsertAfter(span, insertAfterNode);
            else
            {
                if (parent.HasChildNodes)
                    parent.InsertBefore(span, parent.ChildNodes[0]);
                else
                    parent.AppendChild(span);
            }

            if (appendAfterSpanNode != null)
                parent.InsertAfter(appendAfterSpanNode, span);


            return span;
        }

        private XmlNode EncloseInDiv(List<XmlNode> nodesForDiv, XmlDocument doc, XmlNode parent, List<XmlAttribute> attribs)
        {

            // put a div around everything in nodesForDiv
            XmlElement div = doc.CreateElement("div");
            foreach (XmlAttribute attrib in attribs)
            {
                XmlAttribute newAttrib = doc.CreateAttribute(attrib.Name);
                newAttrib.Value = attrib.Value;
                div.Attributes.Append(newAttrib);
            }
            System.Diagnostics.Debug.Assert(nodesForDiv.Count > 0);
            XmlNode insertAfterNode = nodesForDiv[0].PreviousSibling;

            foreach (var n in nodesForDiv)
            {
                parent.RemoveChild(n);
                div.AppendChild(n);
            }

            if (insertAfterNode != null)
                parent.InsertAfter(div, insertAfterNode);
            else
            {
                if (parent.HasChildNodes)
                    parent.InsertBefore(div, parent.ChildNodes[0]);
                else
                    parent.AppendChild(div);
            }


            return div;
        }


        private bool IsBlockElm(XmlNode node)
        {
            if (node.NodeType != XmlNodeType.Element)
                return false;

            return !EblaHelpers.IsInlineTag(node.Name);
        }

        
        private bool IsAncestor(XmlNode node, XmlNode putativeAncestor)
        {
            if (node.ParentNode == null)
                return false;

            if (object.ReferenceEquals(putativeAncestor, node.ParentNode))
                return true;

            return IsAncestor(node.ParentNode, putativeAncestor);

        }

        private XmlElement InsertStartSegElm(XmlDocPos pos, XmlDocument doc, List<XmlAttribute> attribs, bool forEdit)
        {
            XmlElement elm = CreateSegElm(doc, attribs, "[", forEdit);
            InsertSegElm(elm, pos, doc);
            return elm;
        }
        private XmlElement InsertEndSegElm(XmlDocPos pos, XmlDocument doc, List<XmlAttribute> attribs, bool forEdit)
        {
            XmlElement elm = CreateSegElm(doc, attribs, "]", forEdit);
            InsertSegElm(elm, pos, doc);
            return elm;
        }

        private XmlElement CreateSegElm(XmlDocument doc, List<XmlAttribute> attribs, string content, bool forEdit)
        {
            XmlElement segElm = doc.CreateElement(forEdit ? Corpus._segMarkerEditPlaceholder : "span");
            foreach (var a in attribs)
            {
                XmlAttribute attrib = doc.CreateAttribute(a.Name);
                attrib.Value = a.Value;
                segElm.Attributes.Append(attrib);
            }
            XmlText text = doc.CreateTextNode(content);
            segElm.AppendChild(text);
            return segElm;
        }



        private void InsertSegElm(XmlElement segElm, XmlDocPos pos, XmlDocument doc)
        {
            if (pos.offset == 0)
            {
                pos.textContent.ParentNode.InsertBefore(segElm, pos.textContent);
            }
            else if (pos.offset == pos.textContent.Length)
            {
                pos.textContent.ParentNode.InsertAfter(segElm, pos.textContent);

            }
            else
            {
                XmlText textAfterTag = doc.CreateTextNode(pos.textContent.Value.Substring(pos.offset));
                pos.textContent.Value = pos.textContent.Value.Substring(0, pos.offset);
                pos.textContent.ParentNode.InsertAfter(segElm, pos.textContent);
                pos.textContent.ParentNode.InsertAfter(textAfterTag, segElm);
            }


        }

        
        internal static int CreateSegmentDefinition(int docid, SegmentDefinition NewSegmentDefinition, MySqlCommand cmd)
        {
            int newSegID = 0;

            cmd.Parameters.Clear();
            cmd.CommandText = "INSERT INTO segmentdefinitions (StartPosition, Length, DocumentID) VALUES (@startpos, @length, @docid)";


            cmd.Parameters.AddWithValue("startpos", NewSegmentDefinition.StartPosition);
            cmd.Parameters.AddWithValue("docid", docid);
            cmd.Parameters.AddWithValue("length", NewSegmentDefinition.Length);

            cmd.ExecuteNonQuery();

            newSegID = EblaHelpers.GetLastInsertID(cmd);


            if (NewSegmentDefinition.Attributes != null)
                if (NewSegmentDefinition.Attributes.Length > 0)
                {

                    cmd.CommandText = "INSERT INTO segmentattributes (SegmentDefinitionID, Name, Value) VALUES (@segdefid, @name, @value)";
                    cmd.Parameters.AddWithValue("segdefid", newSegID);
                    MySqlParameter nameParm = cmd.Parameters.AddWithValue("name", "");
                    MySqlParameter valueParm = cmd.Parameters.AddWithValue("value", "");

                    foreach (SegmentAttribute attrib in NewSegmentDefinition.Attributes)
                    {
                        nameParm.Value = attrib.Name;
                        valueParm.Value = attrib.Value;
                        cmd.ExecuteNonQuery();
                    }
                }


            
            return newSegID;
            


        }

        public int CreateSegmentDefinition(SegmentDefinition NewSegmentDefinition)
        {
            CheckNotEmpty();
            CheckCanWrite(_corpusname);

            CheckPosValues(NewSegmentDefinition.StartPosition, NewSegmentDefinition.Length);


            int newSegID = 0;

            using (MySqlTransaction txn = GetConnection().BeginTransaction())
            {
                using (MySqlCommand cmd = new MySqlCommand("", GetConnection()))
                {
                    newSegID = CreateSegmentDefinition(_id, NewSegmentDefinition, cmd);
                }

                txn.Commit();
                return newSegID;
            }

            
        }



        public void UpdateSegmentDefinition(SegmentDefinition UpdatedSegmentDefinition)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            SegmentDefinition current = GetSegmentDefinition(UpdatedSegmentDefinition.ID);

            if (current == null)
                throw new Exception("SegmentDefinition with ID " + UpdatedSegmentDefinition.ID.ToString() + " not found.");

            CheckPosValues(UpdatedSegmentDefinition.StartPosition, UpdatedSegmentDefinition.Length);

            List<SegmentAttribute> attribsToDelete = new List<SegmentAttribute>();
            List<SegmentAttribute> attribsToInsert = new List<SegmentAttribute>();
            List<SegmentAttribute> attribsToUpdate = new List<SegmentAttribute>();

            SegmentAttribute[] newAttribs = UpdatedSegmentDefinition.Attributes;
            if (newAttribs == null)
                newAttribs = new SegmentAttribute[0];

            foreach (var a in current.Attributes)
            {
                bool found = false;
                foreach (var a2 in newAttribs)
                {
                    if (string.Compare(a.Name, a2.Name) == 0)
                    {
                        found = true;
                        if (string.Compare(a.Value, a2.Value) != 0)
                            attribsToUpdate.Add(a2);
                        break;
                    }
                }

                if (!found)
                    attribsToDelete.Add(a);
            }

            foreach (var a in newAttribs)
            {
                bool found = false;
                foreach (var a2 in current.Attributes)
                {
                    if (string.Compare(a.Name, a2.Name) == 0)
                    {
                        found = true;
                        break;

                    }
                }
                if (!found)
                    attribsToInsert.Add(a);
            }

            string sql = "UPDATE segmentdefinitions SET StartPosition = @startpos, Length = @length WHERE ID = " + UpdatedSegmentDefinition.ID.ToString();

            using (MySqlTransaction txn = GetConnection().BeginTransaction())
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
                {
                    cmd.Transaction = txn;

                    cmd.Parameters.AddWithValue("startpos", UpdatedSegmentDefinition.StartPosition);
                    cmd.Parameters.AddWithValue("length", UpdatedSegmentDefinition.Length);

                    cmd.ExecuteNonQuery();

                    cmd.Parameters.Clear();

                    MySqlParameter nameParm = cmd.Parameters.AddWithValue("name", "");
                    MySqlParameter valueParm = cmd.Parameters.AddWithValue("value", "");
                    cmd.Parameters.AddWithValue("segdefid", UpdatedSegmentDefinition.ID);
                    cmd.CommandText = "INSERT INTO segmentattributes (SegmentDefinitionID, Name, Value) VALUES (@segdefid, @name, @value)";

                    foreach (var a in attribsToInsert)
                    {
                        nameParm.Value = a.Name;
                        valueParm.Value = a.Value;

                        cmd.ExecuteNonQuery();
                    }

                    cmd.CommandText = "UPDATE segmentattributes SET Value = @value WHERE Name = @name AND SegmentDefinitionID = @segdefid";
                    foreach (var a in attribsToUpdate)
                    {
                        nameParm.Value = a.Name;
                        valueParm.Value = a.Value;

                        cmd.ExecuteNonQuery();

                    }

                    cmd.CommandText = "DELETE FROM segmentattributes WHERE Name = @name AND SegmentDefinitionID = @segdefid";
                    foreach (var a in attribsToDelete)
                    {
                        nameParm.Value = a.Name;

                        cmd.ExecuteNonQuery();

                    }
                }



                txn.Commit();
            }
        }

        public SegmentDefinition GetSegmentDefinition(int ID)
        {
            CheckCanRead(_corpusname);

            return GetSegmentDefinition(ID, _id, GetConnection(), false);

        }

        static internal SegmentDefinition GetSegmentDefinition(int ID, int docID, MySqlConnection cn, bool includeInternal)
        {
            string filter = " AND segmentdefinitions.ID = " + ID.ToString() + " ";
            string sql = "SELECT segmentdefinitions.ID, StartPosition, Length, Name, Value FROM segmentdefinitions LEFT OUTER JOIN segmentattributes ON segmentdefinitions.ID = segmentattributes.SegmentDefinitionID  WHERE DocumentID = " + docID.ToString() + filter + " ORDER BY segmentdefinitions.ID";

            List<SegmentDefinition> segDefs = ReadSegDefs(sql, cn, includeInternal);


            if (segDefs.Count == 0)
                return null;

            System.Diagnostics.Debug.Assert(segDefs.Count == 1);

            return segDefs[0];
        }


        static private void AppendAttribFromReader(SegmentDefinition segDef, MySqlDataReader dr, int nameOrdinal, int valueOrdinal, bool includeInternal)
        {
            SegmentAttribute attrib = new SegmentAttribute();

            attrib.Value = dr.GetString(valueOrdinal);
            attrib.Name = dr.GetString(nameOrdinal);

            // SKip anything beginning with ':' - we 'reserve' that for internal use (e.g. eddy/viv)
            if (!includeInternal)
                if (!string.IsNullOrWhiteSpace(attrib.Name))
                    if (attrib.Name[0] == ':')
                        return;

            Array.Resize(ref segDef.Attributes, segDef.Attributes.Length + 1);
            segDef.Attributes[segDef.Attributes.Length - 1] = attrib; // AttribFromReader(dr, nameOrdinal, valueOrdinal);


        }

        void CheckArgPositive(int arg, string name)
        {
            if (arg < 0)
                throw new ArgumentException("Value may not be negative", name);

        }

        internal static string AppendFilter(string oldfilter, string toAppend, bool And)
        {
            if (oldfilter.Length > 0)

                oldfilter += And ? " AND " : " OR ";

            oldfilter += "(" + toAppend + ")";
            return oldfilter;
        }

        private void CheckPosValues(int StartPosition, int Length)
        {

            CheckArgPositive(StartPosition, "StartPosition");
            CheckArgPositive(Length, "Length");

            int endPosition = StartPosition + Length;
            if ((endPosition) > _length)
                throw new Exception("Values exceed document length.");

        }

        internal static string SegdefBoundsFilter(int StartPosition, int Length, bool includeStartBefore, bool includeEndAfter, string tablePrefix)
        {
            string filter = string.Empty;
            int endPosition = StartPosition + Length;
            // First construct filter assuming neither flag set

            if (StartPosition > 0)
                filter = AppendFilter(filter, tablePrefix + "StartPosition >= " + StartPosition.ToString(), true);

            if (true) //endPosition < _length)
                filter = AppendFilter(filter, "(" + tablePrefix + "StartPosition + " + tablePrefix + "Length) <= " + endPosition.ToString(), true);

            if (includeStartBefore && includeEndAfter)
            {
                filter = string.Empty;

                filter = AppendFilter(filter, "(" + tablePrefix + "StartPosition + " + tablePrefix + "Length) BETWEEN " + StartPosition.ToString() + " AND " + endPosition.ToString(), true);

                filter = AppendFilter(filter, tablePrefix + "StartPosition BETWEEN " + StartPosition.ToString() + " AND " + endPosition.ToString(), false);

                //filter = AppendFilter(filter, "StartPosition < " + FromPosition.ToString() + " AND (StartPosition + Length) > " + endPosition.ToString(), false);

            }
            else if (includeStartBefore)
            {
                filter = string.Empty;

                filter = AppendFilter(filter, "(" + tablePrefix + "StartPosition + " + tablePrefix + "Length) BETWEEN " + StartPosition.ToString() + " AND " + endPosition.ToString(), true);


            }
            else if (includeEndAfter)
            {
                filter = string.Empty;
                filter = AppendFilter(filter, tablePrefix + "StartPosition BETWEEN " + StartPosition.ToString() + " AND " + endPosition.ToString(), true);

            }

            return filter;

        }

        public SegmentDefinition[] FindSegmentDefinitions(int StartPosition, int Length, bool includeStartBefore, bool includeEndAfter, SegmentAttribute[] AttributeFilters)
        {
            return FindSegmentDefinitions(StartPosition, Length, includeStartBefore, includeEndAfter, AttributeFilters, false);
        }

        private SegmentDefinition[] FindSegmentDefinitions(int StartPosition, int Length, bool includeStartBefore, bool includeEndAfter, SegmentAttribute[] AttributeFilters, bool includeInternal)
        {
            CheckOpen();
            CheckCanRead(_corpusname);


            CheckPosValues(StartPosition, Length);
            int endPosition = StartPosition + Length;

            string filter = SegdefBoundsFilter(StartPosition, Length, includeStartBefore, includeEndAfter, string.Empty);


            if (!string.IsNullOrEmpty(filter))
                filter = " AND (" + filter + ") ";

            string sql = "SELECT segmentdefinitions.ID, StartPosition, Length, Name, Value FROM segmentdefinitions LEFT OUTER JOIN segmentattributes ON segmentdefinitions.ID = segmentattributes.SegmentDefinitionID  WHERE DocumentID = " + _id.ToString()  + filter + " ORDER BY segmentdefinitions.ID";

            List<SegmentDefinition> segDefs = ReadSegDefs(sql, GetConnection(), includeInternal);

            // Could do this in sql, like in AlignmentSet
            if (AttributeFilters != null)
                if (AttributeFilters.Length > 0)
                {
                    List<SegmentDefinition> temp = new List<SegmentDefinition>();
                    foreach (var s in segDefs)
                    {
                        bool include = false;
                        foreach (var f in AttributeFilters)
                        {
                            // Does the seg def have a matching attrib?
                            bool found = false;
                            foreach (var a in s.Attributes)
                            {
                                if (string.Compare(a.Name, f.Name) == 0)
                                {
                                    found = true;
                                    // Does the value match?
                                    include = string.Compare(a.Value, f.Value) == 0;

                                    break;
                                }

                            }
                            if (!include || !found)
                                break;
                        }

                        if (include)
                            temp.Add(s);
                    }

                    segDefs.Clear();
                    segDefs.AddRange(temp);
                }

            return segDefs.ToArray();
        }

        static private List<SegmentDefinition> ReadSegDefs(string sql, MySqlConnection cn, bool includeInternal)
        {
            List<SegmentDefinition> segDefs = new List<SegmentDefinition>();

            using (MySqlCommand cmd = new MySqlCommand(sql, cn))
            {
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    SegmentDefinition segDef = null;
                    int idOrdinal = dr.GetOrdinal("ID");
                    int lengthOrdinal = dr.GetOrdinal("Length");
                    int startOrdinal = dr.GetOrdinal("StartPosition");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int valueOrdinal = dr.GetOrdinal("Value");
                    while (dr.Read())
                    {
                        int id = dr.GetInt32(idOrdinal);

                        if (segDef != null)
                        {
                            if (id == segDef.ID)
                            {
                                // Got another attribute for the same seg
                                AppendAttribFromReader(segDef, dr, nameOrdinal, valueOrdinal, includeInternal);
                                continue;
                            }
                        }

                        segDef = new SegmentDefinition();
                        segDef.Attributes = new SegmentAttribute[0];

                        segDef.ID = id;
                        segDef.StartPosition = dr.GetInt32(startOrdinal);
                        segDef.Length = dr.GetInt32(lengthOrdinal);
                        if (!dr.IsDBNull(nameOrdinal))
                            AppendAttribFromReader(segDef, dr, nameOrdinal, valueOrdinal, includeInternal);

                        segDefs.Add(segDef);
                    }

                }
                
            }

            return segDefs;

        }

        public void DeleteSegmentDefinition(int ID)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            string sql = "DELETE FROM segmentdefinitions WHERE DocumentID = " + _id.ToString() + " AND ID = " + ID.ToString();

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteAllSegmentDefinitions()
        {
            CheckOpen();
            CheckCanWrite(_corpusname);
            string sql = "DELETE FROM segmentdefinitions WHERE DocumentID = " + _id.ToString();

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.ExecuteNonQuery();
            }
        }


        internal class CloneStatus
        {
            public int LengthRemaining;
            public bool StartPosFound;
        }

        internal static void CloneBetweenPositions(XmlNode sourceNode, XmlNode targetNode, XmlDocPos startPos, CloneStatus status, bool cloneIds, XmlDocPos.IgnoreElmDelg IgnoreElmImpl)
        {

            foreach (XmlNode sourceChild in sourceNode.ChildNodes)
            {
                XmlNode targetChild = null;
                bool ignore = false;
                switch (sourceChild.NodeType)
                {
                    case XmlNodeType.Text:
                        XmlText sourceText = (XmlText) sourceChild;
                        XmlText targetText = null;
                        if (!(status.StartPosFound))
                        {
                            if (object.ReferenceEquals(sourceText, startPos.textContent))
                            {
                                status.StartPosFound = true;
                                string text = sourceText.Value.Substring(startPos.offset);
                                if (text.Length > status.LengthRemaining)
                                    text = text.Substring(0, status.LengthRemaining);
                                targetText = targetNode.OwnerDocument.CreateTextNode(text);
                                status.LengthRemaining -= text.Length;

                            }
                        }
                        else
                        {
                            if (status.LengthRemaining > 0)
                            {
                                string text = sourceText.Value;
                                if (text.Length > status.LengthRemaining)
                                    text = text.Substring(0, status.LengthRemaining);
                                targetText = targetNode.OwnerDocument.CreateTextNode(text);
                                status.LengthRemaining -= text.Length;
                            }

                        }
                        targetChild = targetText;
                        break;
                    case XmlNodeType.Whitespace:
                        if (status.LengthRemaining > 0)
                        {
                            XmlWhitespace sourceWhitespace = (XmlWhitespace)sourceChild;
                            XmlWhitespace targetWhitespace = targetNode.OwnerDocument.CreateWhitespace(sourceWhitespace.Value);
                            targetChild = targetWhitespace;

                        }
                        break;
                    case XmlNodeType.Element:
                        if (IgnoreElmImpl != null)
                            ignore = IgnoreElmImpl.Invoke(sourceChild);
                        
                        if (!ignore)
                        if (status.LengthRemaining > 0)
                        {
                            XmlElement sourceElm = (XmlElement)sourceChild;
                            XmlElement targetElm = targetNode.OwnerDocument.CreateElement(sourceElm.Name);
                            if (cloneIds)
                            {
                                XmlAttribute attrib = targetNode.OwnerDocument.CreateAttribute("id");
                                attrib.Value = EblaHelpers.GetNodeId(sourceElm).ToString();
                                targetElm.Attributes.Append(attrib);
                            }
                            foreach (XmlAttribute attrib in sourceElm.Attributes)
                            {
                                if (string.Compare(attrib.Name, "id") != 0)
                                {
                                    XmlAttribute a = targetElm.OwnerDocument.CreateAttribute(attrib.Name);
                                    a.Value = attrib.Value;
                                    targetElm.Attributes.Append(a);

                                }
                            }
                            targetChild = targetElm;
                        }
                        break;
                    default:
                        throw new Exception("Unexpected XmlNodeType in Clone: " + sourceChild.NodeType.ToString());
                }

                if (!ignore)
                CloneBetweenPositions(sourceChild, targetChild, startPos, status, cloneIds, IgnoreElmImpl);

                if (targetChild != null && status.StartPosFound)
                    targetNode.AppendChild(targetChild);

                if (status.LengthRemaining == 0)
                    break;

            }
        }

       
        private void CheckNotEmpty()
        {
            if (_content == null)
                throw new Exception("The document is empty.");
        }

        HashSet<string> _inlineTags = new HashSet<string>() { "a", "abbr", "acronym", "b", "basefont", "bdo", "big", 
                                "br", "cite", "code", "dfn", "em", "font", "i",
                                "img", "input", "kbd", "label", "q", "s", "samp", "select", "small",
                                "span", "strike", "strong", "sub", "sup", "textarea", "tt", "u", "var", 
                                "ins", "del", "applet", "button", "iframe", "map", "object", "script"}; // Strictly speaking, these can be used as block-level and 
                                // contain other block-level elements. See comments within the code that uses 
                                // this HashSet.


    }
}
