﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using EblaAPI;

namespace EblaImpl
{
    /// <summary>
    /// Class that can be used by a host program to expose Ebla via WCF.
    /// </summary>
    public sealed class EblaServiceControl
    {

        private EblaServiceControl() { }

        public static EblaServiceControl Instance
        {

            get
            {
                return instance;
            }


        }

        public bool Started
        {
            get
            {
                return _started;
            }
        }

        private bool? useHttp;
        public bool UseHttp()
        {
            if (!useHttp.HasValue)
            {
                string s = _configProvider["UseHttp"];

                useHttp = Boolean.Parse(s);

            }

            return useHttp.Value;

        }


        public void Start(System.Collections.Specialized.NameValueCollection configProvider, Type corpusStoreType)
        {
            _configProvider = configProvider;
            if (_started)
                throw new Exception("EblaService is already running.");

            if (!(typeof(ICorpusStore).IsAssignableFrom(corpusStoreType)))
                throw new Exception(corpusStoreType.Name + " does not implement ICorpusStore.");


            string serverName = configProvider["ServerName"];

            if (UseHttp())
            {
                _corpusStoreHost = new ServiceHost(corpusStoreType);

                Uri baseAddress = new Uri(EblaHelpers.CorpusStoreBaseAddressHttp(serverName));
                WSHttpBinding wsBinding = new WSHttpBinding();
                wsBinding.MaxReceivedMessageSize = 500000;
                _corpusStoreHost.AddServiceEndpoint(typeof(ICorpusStore), wsBinding, EblaHelpers.CorpusStoreBaseAddressHttp(serverName));


            }
            else
            {

                Uri baseAddress = new Uri(EblaHelpers.CorpusStoreBaseAddressNetTcp(serverName));
                _corpusStoreHost = new ServiceHost(corpusStoreType, baseAddress);
                NetTcpBinding binding = new NetTcpBinding();
                _corpusStoreHost.AddServiceEndpoint(typeof(ICorpusStore), binding, "");

                //ServiceMetadataBehavior metadataBehavior;
                //metadataBehavior = _liftServiceHost.Description.Behaviors.Find<ServiceMetadataBehavior>();
                //if(metadataBehavior == null)
                //{ 
                //  metadataBehavior = new ServiceMetadataBehavior();
                //  metadataBehavior.HttpGetEnabled = true;
                //  _liftServiceHost.Description.Behaviors.Add(metadataBehavior);
                //}
                //_liftServiceHost.AddServiceEndpoint(typeof(IMetadataExchange), binding, "MEX");


                

            }

            _corpusStoreHost.Open();

            _started = true;
        }


        public void Stop()
        {
            if (!_started)
                throw new Exception("EblaService is not running.");

            _corpusStoreHost.Close();

            _started = false;
        }


        private static readonly EblaServiceControl instance = new EblaServiceControl();
        private bool _started = false;

        ServiceHost _corpusStoreHost = null;

        System.Collections.Specialized.StringDictionary _configValues = new System.Collections.Specialized.StringDictionary();

        private System.Collections.Specialized.NameValueCollection _configProvider;

        private static readonly object _locker = new object();


    }
}
