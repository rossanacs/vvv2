﻿

var EblaNS = EblaNS || {};


EblaNS.VariationMetricTypes = {
    metricA: { value: 1, name: 'A - Eddy=Euclidean distance, Viv=average Eddy' },
    metricB: { value: 2, name: 'B - Original Viv and Eddy formulae' },
    metricC: { value: 3, name: 'C - Eddy=Euclidean distance, Viv=S.D. of Eddy' }
};

EblaNS.FillVariationTypeSelectElm = function (elmselector) {
    var $vs = $(elmselector);

    _.each(EblaNS.VariationMetricTypes, function (type, value) {
        $vs.append('<option value="' + type.value + '">' + type.name + '</option>');
    });

}