﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

var PrismNS = PrismNS || {};


/////////////////
//
//  PrismNS.Utils
//
//  Helper functions that don't yet belong anywhere else
//
/////////////////

PrismNS.Utils = {};

PrismNS.Utils.GetCorpusName = function () {
    if (_corpusName)
        return _corpusName;

    _corpusName = $('#corpusname').val();
    return _corpusName;
}

PrismNS.Utils.GetSegmentIDForNode = function (node) {
    var idstring = $(node).attr("data-eblasegid");
    if (idstring) {
        if (idstring.length > 0) {

            var id = parseInt(idstring);
            if (id > 0)
                return id;
        }

    }

    if (node.parentNode == null)
        return -1;

    return PrismNS.Utils.GetSegmentIDForNode(node.parentNode);

}

PrismNS.Utils.EnableButton = function(id, enabled) {
    if (enabled) {
        $('#' + id).removeAttr('disabled');
    }
    else {
        $('#' + id).attr('disabled', 'disabled');
    }
}
