Translation.prototype.createFilter = function( id ) {

    // init layers
    layerFilter[ id ] = new Kinetic.Layer();
    layerSpeaker[ id ] = new Kinetic.Layer();

    var names = new Array();
    //reset value
    sceneLength = 0;

    // find all divs and save in array
    var speeches = $( this.getA() ).find('blockquote').children("span").parent().children().not("i").parent();
    var speechesCount = speeches.length;

	// count all text length
	var totalTextLength = 0; 
	for(var i = 0; i <= speechesCount -1 ; i++) {
		//save total text length
        totalTextLength +=  $(speeches[i]).text().replace(/ /g,'').length;
        //delete all not speeches
        /*if( $(speeches[i]).children("i").length > 0 ) {
        	console.log($(speeches[i]).children("i"));
        }*/
     }
   
    if(totalTextLength > maxFilterHeight)  maxFilterHeight = totalTextLength;
     var blackSpace = sidebarHeight - ( speechesCount * margin);

    filterStage[ id ] = new Kinetic.Stage({
      container: "filter_canvas_" +id,
      width: filterWidth-20,
      height:   maxFilterHeight / 10
    });
    
    // add for every text item a filter item
    for(var i = 0; i <= speechesCount -1 ; i++) {
    
    	var height = Math.sqrt(blackSpace * $(speeches[i]).text().replace(/ /g,'').length / maxFilterHeight ) * 5;

        speakerBar[ i ] = 0;
        speaker[ i ] = 0;
        
        // add a unique ID for every item
        var speakerID = $(speeches[i]).children("span[data-eblasegid]:first").attr('data-eblasegid'); // segment ID!
        var speakerName = $( speeches[i] ).children("span[data-userattrib-speaker]:first").attr('data-userattrib-speaker');

        if ( _.isUndefined( speakerName ) ) {
            speakerName = $( speeches[i] ).prev().text();
        }

        translation[ id ].pushAllEblaIDs(speakerID);
        
        // count text length, get all p tags convert it to text (remove all html tags) and delete all white spaces
        // optimize shapes to fit screen
        //find the longest text 
        if(totalTextLength > maxFilterHeight) {
        	maxFilterHeight = totalTextLength;
        	filterScale = (totalTextLength / filterHeight);
        }

        //var speechLength = Math.pow( $(speeches[i]).text().replace(/ /g,'').length , filterScale); 

        if ( _.isUndefined( speakerName ) ) {
            names[i] = '';
        }
        else {
            names[i] = speakerName;
        }
        
        // init rect shape
        speakerBar[ speakerID ] = new Kinetic.Rect({   
          x: 0,
          y: sceneLength,
          width: filterBarWidthMax,
          height: height,
          fill: filterNormal,
          strokeWidth: filterStrokeWidth,
          stroke: filterStrokeWidthColor,
          index: 1,
          cornerRadius: 0,
          name: names[i],
          id: speakerID
        });

        // init text object
        speaker[ speakerID ] = new Kinetic.Text({
            text: "",
            fontFamily: fontFilter,
            fontSize: 10,
            padding: 6,
            textFill: "white",
            alpha: 1,
            fill: '#2d2d2d',
            visible: false,
            index: 2
        });

        
        // event listener
        speakerBar[ speakerID ].on("mousemove", function() {
            document.body.style.cursor = "pointer";

            var sID = this.getID();
            
            if( (this.getFill() == filterNormal) || (this.getFill() == filterHover) || (this.getFill() == filterSelected) )  {this.setFill(filterHover);}
            else this.setFill(filterHoverHighlighted);
            
            this.setStrokeWidth(filterStrokeWidthHover);
            this.setStroke(filterStrokeWidthColorHover);
            
            // style active rect
            if(translation[ id ].getActiveFilterItem()) { translation[ id ].getActiveFilterItem().setFill(filterSelected); }
            layerFilter[ id ].draw();

            var mousePos = filterStage[ id ].getMousePosition();
            if ( !_.isUndefined( mousePos ) ) { speaker[ sID ].setPosition(mousePos.x - 20, mousePos.y + 20); }
            speaker[ sID ].setText(this.getName());
            speaker[ sID ].show();
            layerSpeaker[ id ].draw();
        });
        
        speakerBar[ speakerID ].on("mouseout", function() {
            //var sID = 'speaker_' + id + '_' + this.getID();
            var sID = this.getID();
            
            document.body.style.cursor = "default";
            if( (this.getFill()==filterHover) || (this.getFill()==actBarFill) ) this.setFill(filterNormal);
            else this.setFill(filterHighlighted);

            // style active rect
            if(translation[ id ].getActiveFilterItem()) { translation[ id ].getActiveFilterItem().setFill(filterSelected); }
            layerFilter[ id ].draw();

            speaker[ sID ].hide();
            if(speakerActive) speakerActive.show();
            layerSpeaker[ id ].draw();
        });


        speakerBar[ speakerID ].on("click", function(e) {
            var sID = this.getID();
            document.body.style.cursor = "pointer";
            e.preventDefault();

            for(var i = 0; i < translation.length; i++) {
            	//reset old selected
                if(translation[i].getActiveTextItem()) $( translation[i].getActiveTextItem() ).parent().removeClass('textSelected');
                if(translation[i].getActiveFilterItem()) translation[i].getActiveFilterItem().setFill(filterNormal);
                if(translation[i].getActiveNameItem()) translation[i].getActiveNameItem().hide();

                //save new selected
                translation[i].setActiveTextItem( 'span[data-eblasegid='+sID+']' );
                translation[i].setActiveFilterItem( this );
                translation[i].setActiveNameItem( speaker[ sID ] );
                lastActiveFilterItem = this;

				//style new selected
                $( translation[i].getActiveTextItem() ).parent().addClass('textSelected');
                this.setFill(filterSelected);
                speaker[ sID ].show();
                   
				//draw layers again
                layerFilter[ i ].draw();
                layerSpeaker[ i ].draw();
       
                //scroll to text part
            	//$('.txt_'+i+' span[data-eblasegid='+sID+']').ScrollTo({ duration: 3000, easing: 'swing'});
            	$('.txt_'+i+' span[data-eblasegid='+sID+']').ScrollTo({ duration: scrollDuration, easing: scrollStyle});

            }
             //show connection
            createConnector( id, sID);
        });

        // move y value
        sceneLength += height + margin;       

        // add the obects to the layers
        layerFilter[ id ].add(speakerBar[ speakerID ]);
        layerSpeaker[ id ].add(speaker[ speakerID ]);

    }

    //add filter name
    var result = _.uniq( _.map( names, function( name ) { return name.replace(/^\W+|\W+$/g,''); } ) );
    
    var create = '<select id="filterNames_'+id+'" class="filterNames customSelect">';
    create += '<option value="none">All speakers</option>'
    for(var i = 0; i < result.length;i++) { create += '<option value="'+result[i]+'">'+result[i]+'</option>'; }
    create += '</select>';
    $('#filter_canvas_'+id).append(create);
 
    $('#filterNames_'+id).change(function(){
            $(this).find('option:selected').each(function () { 
                    //mark selected speakers    
             		var eblaID = translation[id].getAllEblaIDs();
             		
                    if( $(this).text() != 'All speakers') {	
                        for(var j=0; j<eblaID.length; j++){
                            var name = speakerBar[ eblaID[j] ].getName().replace(/^\W+|\W+$/g,'');
                            if(name == $(this).text()){
                                speakerBar[ eblaID[j] ].setFill(filterHighlighted );
                                speakerBar[ eblaID[j] ].setWidth(filterBarWidthMax);
                            }
                            else { 
                           	  speakerBar[ eblaID[j] ].setWidth(filterBarWidthMin); 
                              speakerBar[ eblaID[j] ].setFill(filterNormal);
                           	 
                            }
                        }
                    }
                    else {  
                        //reset name filter
                        for(var j=0; j<eblaID.length; j++){
                            speakerBar[ eblaID[j] ].setFill(filterNormal);
                            speakerBar[ eblaID[j] ].setWidth(filterBarWidthMax);
                        }
                    }

                    layerFilter[ id ].draw();
                    layerSpeaker[ id ].draw();

            });
    });

	// add filter order
    var create = '<select id="filterOrder_'+id+'" class="filterOrder customSelect">';
    create += '<option value="chronologic">text flow</option>';
    create += '<option value="text length">text length</option>';
    create += '<option value="speaker">speaker</option>';
    create += '</select>';
    $('#filter_canvas_'+id).append(create);

    $('#filterOrder_'+id).change(function(){
            $(this).find('option:selected').each(function () {
                    var sortVal = $(this).text();
                    var eblaID = translation[id].getAllEblaIDs();   
                    
                        var jsonObj = []; //declare array
                        for(var j=0; j<eblaID.length; j++){ 
                            jsonObj.push({num: j, id: eblaID[j] , height: speakerBar[ eblaID[j] ].getHeight(), name: speakerBar[ eblaID[j] ].getName()});
                        }
                        
                        //sort functions
                         function sortTextLength(a,b){  
                             if (a.height == b.height){
                               return 0;
                             }
                             return a.height > b.height ? 1 : -1;  
                         };  
                          function sortTextLengthDesc(a,b){  
                             return sortTextLength(a,b) * -1;  
                         };
                         
                        function sortChron(a,b){  
                             if (a.num == b.num){
                               return 0;
                             }
                             return a.num < b.num ? 1 : -1;  
                         };  
                         
                         function sortChronAsc(a,b) {
                            return sortChron(a,b) * -1;  
                         }
                         
                        function sortSpeaker(a,b){  
                             if (a.name == b.name){
                               return 0;
                             }
                             return a.name < b.name ? 1 : -1;  
                         };  
                         
                         function sortSpeakerAsc(a,b) {
                            return sortSpeaker(a,b) * -1;  
                         }
                         
    
                        var filterName = $('#filterNames_'+id+' option:selected').val();

                        //if(filterName == 'none') {
                            if(sortVal == 'text length') {
                            
                                var sorted = $(jsonObj).sort(sortTextLengthDesc);
                                var cHeight = 0;
            
                                for(var j=0; j<sorted.length; j++){ 
                                    speakerBar[ sorted[j].id ].setPosition(0,cHeight);
                                    cHeight += sorted[j].height + margin;
                                    
                                }
                            }
                            else if(sortVal == 'text flow') {
                                var sorted = $(jsonObj).sort(sortChronAsc);
                                var cHeight = 0;
            
                                for(var j=0; j<sorted.length; j++){ 
                                    speakerBar[ sorted[j].id ].setPosition(0,cHeight);
                                    cHeight += sorted[j].height + margin;
                                    
                                } 
                            }
                            else if(sortVal == 'speaker') {
                                var sorted = $(jsonObj).sort(sortSpeakerAsc);
                                var cHeight = 0;
            
                                for(var j=0; j<sorted.length; j++){ 
                                    speakerBar[ sorted[j].id ].setPosition(0,cHeight);
                                    cHeight += sorted[j].height + margin;
                                    
                                } 
                            }
                       /* }
                        else { 
                            // a speaker is selected
                            if(sortVal == 'text length') {
                            
                                var sorted = $(jsonObj).sort(sortTextLengthDesc);
                                var cHeight = 0;
            
                                for(var j=0; j<sorted.length; j++){ 
                                    speakerBar[ sorted[j].id ].setPosition(0,cHeight);
                                    cHeight += sorted[j].height + margin;
                                    
                                }
                            }
                            else if(sortVal == 'chronologic') {
                                var sorted = $(jsonObj).sort(sortChronAsc);
                                var cHeight = 0;
            
                                for(var j=0; j<sorted.length; j++){ 
                                    speakerBar[ sorted[j].id ].setPosition(0,cHeight);
                                    cHeight += sorted[j].height + margin;
                                    
                                } 
                            }
                            else if(sortVal == 'speaker') {
                                var sorted = $(jsonObj).sort(sortSpeakerAsc);
                                var cHeight = 0;
            
                                for(var j=0; j<sorted.length; j++){ 
                                    speakerBar[ sorted[j].id ].setPosition(0,cHeight);
                                    cHeight += sorted[j].height + margin;
                                    
                                } 
                            }
                        }*/
                    layerFilter[ id ].draw();
                    layerSpeaker[ id ].draw();
            });
            

      
      
    });
    
    // add the layer to the stage
    filterStage[ id ].add(layerFilter[ id ]);
    filterStage[ id ].add(layerSpeaker[ id ]);


	//event triggered when clicked on text
     $('span[data-eblatype=segformat][data-userattrib-type=Speech]').unbind().bind('click', function() {
     
		//get only first span
		var sID = $(this).parent().children('span').attr('data-eblasegid');
     
         //reset all translations    
      	 for(var i = 0; i < translation.length; i++) {
                if(translation[i].getActiveTextItem()) $( translation[i].getActiveTextItem() ).parent().removeClass('textSelected');
                if(translation[i].getActiveFilterItem()) translation[i].getActiveFilterItem().setFill(filterNormal);
                if(translation[i].getActiveNameItem()) translation[i].getActiveNameItem().hide();                                              
          }
       
		//get current txt id
		var ids = $(this).closest('div').parent().parent().attr('class');
		id = ids.substring(ids.length-1, ids.length);

        //save new selected
        translation[id].setActiveTextItem( 'span[data-eblasegid='+sID+']' );
        translation[id].setActiveFilterItem( speakerBar[ sID ] );
        translation[id].setActiveNameItem( speaker[ sID ] );
        lastActiveFilterItem = speakerBar[ sID ];

		//style new selected
        $(translation[id].getActiveTextItem() ).parent().addClass('textSelected');
        translation[id].getActiveFilterItem().setFill(filterSelected);
        //speaker[ sID ].show();
           
		//draw layers again
        layerFilter[ id ].draw();
        layerSpeaker[ id ].draw();
        
        //scroll to text part
    	$('span[data-eblasegid='+sID+']').ScrollTo({ duration: scrollDuration, easing: scrollStyle}); 
    	
      	//show connection
        createConnector( id, sID); 
       
      });
      
}