﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Version Variation Visualization - About
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">
    	<div class="row">
			<div class="span12">
				<iframe src="https://docs.google.com/spreadsheet/embeddedform?formkey=dGRHdjhwZWFvN0NIemN6eE5vZ2lxVWc6MQ" width="100%" height="700" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
			</div>
		</div>
	</div>
</asp:Content>
