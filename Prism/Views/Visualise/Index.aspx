<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Parallel View
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <div id="container"></div>

	<div class="container-fluid visualise">
    	<div id="panel">
    		<div class="row-fluid">
    			<div class="span6 title"><h3>Base Text <small><%=ViewData["basetextdate"] %></small></h3></div>
    			<div class="span6 title"><h3><%=ViewData["versionname"] %> <small><%=ViewData["versiondate"] %></small></h3></div>
    		</div>
    		<div class="row-fluid">
    			<div id="basetextdiv" class="span6 version txt_0">
    				<div class="toolbar">
	    				<div class="toolHeader pull-right">
	    					<a href="#" class="tt" title="Hide or reveal the vertical navigation tool" data-placement="right">
	    					<button type="button" class="hideFilter visible" id="hideFilter_0" onclick="showHideFilter(0);" ></button>
	    					</a>
	    				</div>
	    				
	    				<div class="tools">
						<p class="desc">Filter/Sort</p>
	    				</div>
	    			</div>
    				<div class="text nano">
	    				<div class="content">
	    					<% Response.Write(ViewData["basetextcontent"].ToString()); %>
	    				</div>
    				</div>
    				<div class="connector" id="con_0"></div>
    			</div>
    			<div id="versiondiv" class="span6 version txt_1" data-versionname="<%=ViewData["versionname"] %>">
    				<div class="toolbar">
	    			   <div class="toolHeader pull-right">
	    			   		<a href="#" class="tt" title="Hide or reveal the vertical navigation tool" data-placement="right">
	    					<button type="button" class="hideFilter visible" id="hideFilter_1" onClick="showHideFilter(1);" ></button>
	    					</a>
	    				</div>
	    			    <div class="tools">
	    			    <p class="desc">Filter/Sort</p>
	    			    </div>
	    			</div>
    				<div class="text nano">
    					<div class="content">
    						<% Response.Write(ViewData["versioncontent"].ToString()); %>
    					</div>
    				</div>
    			
    			</div>
    		</div>
    	</div>
        <input type="hidden" id="corpusname" value="<%=ViewData["corpusname"] %>" />
        <input type="hidden" id="versionname" value="<%=ViewData["versionname"] %>" />
        <input type="hidden" id="selectedsegid" value="-1" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-Formatting.js") %>" type="text/javascript"></script>
	<script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/kinetic.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/jquery.scrollto.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/jquery.sb.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/nanoScroller/jquery.nanoscroller.js") %>" type="text/javascript"></script>

    <script src="<%= Url.Content("~/Scripts/alignView/translation.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/alignView/nav.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/alignView/filter.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/alignView/connector.js") %>" type="text/javascript"></script>

    <link href="<%= Url.Content("~/Content/css/alignView.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/css/nanoscroller.css") %>" rel="stylesheet" type="text/css" />
 	<link href="<%= Url.Content("~/Content/css/customselect.css") %>" rel="stylesheet" type="text/css" />

    <script src="<%= Url.Content("~/Scripts/vvv/fruitmachine.js") %>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Help" ContentPlaceHolderID="HelpMenuItem" runat="server">
<a id="help-popover" href="#"><i class="icon-question-sign icon-white"></i> What&#8217;s this?</a>
<div id="help-content" style="display:none">
	<p>
	<i>Fruit Machine</i>: scroll the texts, click any segment (in Othello, here, a segment is a speech). Base Text and translation re-align automatically.<br /><br />
	<i>Barcode</i>: a bar represents a segment. Its size represents the word-count. The vertical navigation strip gives a text structure overview, and finds segments with defined features, using colours and shape to identify them. 
	</p>
</div>
</asp:Content>