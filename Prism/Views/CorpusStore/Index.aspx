﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.CorpusStoreModel>" %>

<asp:Content ID="SiteSpecificScripts" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-CorpusStore.js") %>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	VVV - Corpus store
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">	
		<div class="page-header">
			<h1>Corpus Store</h1>
			<p><%: Html.ValidationSummary(false) %></p>  
			<p class="pull-right">
                <% if (Prism.Controllers.AccountController.SessionEblaIsAdmin)
                   { %>

		        	<%: Html.ActionLink("Create New", "Create", null, new { @class = "btn" })%> 
		        	<%: Html.ActionLink("Admin", "UsersAdmin", null, new { @class = "btn" })%>
<%--		        	<input type="button" id="backup" value="Backup" />--%>

<% }  %>

		    </p>          
		</div>

		<div class="row">
			<div class="span12">
		    	<table class="table table-bordered">
		        	<tr>
		            	<th>
		                	Corpus name
		            	</th>
		            	<th>
		                	Description
		            	</th>
		            	<th>
		            	</th>
                        <% if (Prism.Controllers.AccountController.SessionEblaIsAdmin)
                           { %>
		            	<th>
		            	</th>
                        <% } %>
		        	</tr>
		    	<% if (Model.CorpusList != null) foreach (var item in Model.CorpusList) { %>
		        	<tr>
		            	<td>
		                	<%: item.CorpusName %>
		            	</td>
		            	<td>
		                	<%: item.Description %>
		            	</td>
		            	<td>
							<%: Html.ActionLink( "Open", "Index", "Corpus", new { CorpusName = item.CorpusName }, new { @class="btn btn-primary" } ) %>
		            	</td>
                        <% if (Prism.Controllers.AccountController.SessionEblaIsAdmin)
                           { %>
		            	<td>
		                	<%: Html.ActionLink("Delete", "Delete", new { CorpusName = item.CorpusName }, new { @class="btn btn-danger", onclick = "return confirm('Are you sure you wish to delete this corpus?');" })%>
		            	</td>
                        <% } %>
		        	</tr>
		    	<% } %>
		    	</table>
			</div>
		</div>
		<div id="push"></div>
	</div>
</asp:Content>

