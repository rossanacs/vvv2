﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.UserRightsModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - User details
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h1>
                User details</h1>
            <p>
                <%: Html.ValidationSummary(false) %></p>
        </div>
        <div class="row">
            <% using (Html.BeginForm())
               {%>
            <%: Html.ValidationSummary(true) %>
            <fieldset>
                <legend>Main details</legend>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Username) %>
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.Username) %>
                    <%: Html.ValidationMessageFor(model => model.Username) %>
                </div>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.CanAdmin) %>
                </div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.CanAdmin) %>
                    <%: Html.ValidationMessageFor(model => model.CanAdmin) %>
                </div>
                <div class="editor-label">
                    <%: Html.LabelFor(m => m.Email) %>
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(m => m.Email) %>
                    <%: Html.ValidationMessageFor(m => m.Email) %>
                </div>

                <fieldset>
                    <legend>Corpus rights</legend>
                    <table>
                        <thead>
                            <tr>
                                <th>
                                    Corpus
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                    
                   //foreach (Prism.Models.UserCorpusRightsModel m in Model.CorpusRights)
                       for (int i = 0; i < Model.CorpusRights.Length; i++)
                   { %>
                            <tr>
                                <td>
                                <input type="hidden" name="CorpusRights.Index" value="<%: i.ToString() %>" />
                                    <%: Model.CorpusRights[i].CorpusName %>
                                <%: Html.HiddenFor(x => Model.CorpusRights[i].CorpusName, new {@name = "CorpusRights[" + i.ToString() + "].CorpusName"}) %>

                                </td>
                                <td>
                                    <%--             <%: Html.EditorFor(x => m) %> --%>
                                    <%: Html.RadioButtonFor(x => Model.CorpusRights[i].RightsValue, 0, new {@name = "CorpusRights[" + i.ToString() + "].RightsValue"})%>
                                    No access
                                </td>
                                <td>
                                    <%: Html.RadioButtonFor(x => Model.CorpusRights[i].RightsValue, 1, new { @name = "CorpusRights[" + i.ToString() + "].RightsValue" })%>
                                    Can read
                                </td>
                                <td>
                                    <%: Html.RadioButtonFor(x => Model.CorpusRights[i].RightsValue, 3, new { @name = "CorpusRights[" + i.ToString() + "].RightsValue" })%>
                                    Can edit
                                </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </fieldset>
                <p>
                    <input type="submit" value="Save" />
                </p>
            </fieldset>
            <% } %>
            <div>
                <%: Html.ActionLink("Back to List", "UsersAdmin") %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
