﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.NewCorpusModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	VVV - Create corpus
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">
		<div class="page-header">
			<h2>Create corpus</h2>
		</div>
		<div class="row">
			<div class="well span12">
		    	<% using (Html.BeginForm()) {%>
		        <%: Html.ValidationSummary(false) %>
		
			        <fieldset>
			            <legend>Details</legend>
			
			            <div class="editor-label">
			                <%: Html.LabelFor(model => model.CorpusName)%>
			            </div>
			            <div class="editor-field">
			                <%: Html.TextBoxFor(model => model.CorpusName)%>
			                <%: Html.ValidationMessageFor(model => model.CorpusName)%>
			            </div>
			
			            <div class="editor-label">
			                <%: Html.LabelFor(model => model.Description)%>
			            </div>
			            <div class="editor-field">
			                <%: Html.TextBoxFor(model => model.Description)%>
			                <%: Html.ValidationMessageFor(model => model.Description)%>
			            </div>
			            
			             <input type="submit" value="Create" />
			        </fieldset>
			    <% } %>
			</div>
		</div>
	    <div class="row">
			<div class="span12">
	        	<%: Html.ActionLink("Back to List", "Index", null, new { @class="btn" } ) %>
			</div>
	    </div>
		<div id="push"></div>
	</div>
</asp:Content>
