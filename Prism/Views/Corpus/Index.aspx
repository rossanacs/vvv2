<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.CorpusModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/vvv/jquery.isotope.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/corpus.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Corpus:
    <%: Model.CorpusName%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="subnav subnav-fixed">
        	<div class="container">
        		<div class="row">
        			<div class="span12">
			            <div class="pull-right">
			            	<% if (Model.CanWrite)
							{ %>
								<div class="btn-group">
								  <a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">
								    Edit Corpus
								    <span class="caret"></span>
								  </a>
								  <ul class="dropdown-menu">
								  	<li><%: Html.ActionLink("Add new version", "CreateVersion", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
								    <li><%: Html.ActionLink("Corpus Admin", "Admin", "Corpus", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
								  </ul>
								</div>
			            	<%} %>
			            </div>
			            <h1><%: Model.CorpusName%> <small><%: Model.Description %></small></h1>
		            </div>
	            </div>
	            <div class="row">
		            <div class="corpus-item base-text span12">
		            	<div class="row">
			                <div class="span7">
				                <h3 class="version-label">Base Text <small>(<%: Model.BaseText.ReferenceDate %>)</small></h3>
				                <div class="description">
				                    <p><%: Model.BaseText.Description %></p>
				                </div>
			                </div>
			                <div class="span5">
			                	<div class="admin pull-right">
				                    <% if (Model.CanWrite)
									   { %>
				                    <div class="btn-group pull-right">
									  <a class="btn btn-mini dropdown-toggle btn-primary" data-toggle="dropdown" href="#">
									    Edit
									    <span class="caret"></span>
									  </a>
									  <ul class="dropdown-menu">
									  	<li><%: Html.ActionLink("View", "Index", "Document", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
										<li><%: Html.ActionLink("Upload", "Upload", "Document", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
					                	<li><%: Html.ActionLink("Details", "DocumentMetadata", "Corpus", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
									  </ul>
									</div>
				                    <% } %>
				                    <a href="<%: Url.Action( "Alignments", "Visualise", new { CorpusName = Model.CorpusName } ) %>" rel="tooltip" data-placement="top" title="<i>Alignment Maps</i>: overviews of deletions and re-ordering" class="btn btn-mini pull-right tt">Alignments</a>
				                    <a href="<%: Url.Action( "EddyOverviewChart", "Charts", new { CorpusName = Model.CorpusName } ) %>" rel="tooltip" data-placement="top" title="Visually compare Eddy values across the text" class="btn btn-mini pull-right tt">Variation</a>
				                    <% if (Model.CorpusName == "Othello, Act 1 Scene 3") { %>
				                    	<a href="<%: Url.Action( "OthelloMap", "Visualise", new { CorpusName = Model.CorpusName } ) %>" rel="tooltip" data-placement="top" title="An interactive map showing German translations from the Othello corpus" class="btn btn-mini pull-right tt">Map</a>
				                    <% } %>
				                    <a href="<%: Url.Action( "EddyHistory", "Charts", new { CorpusName = Model.CorpusName, BaseTextSegmentId = -1 } ) %>" rel="tooltip" data-placement="top" title="Plot Eddy values versus reference date" class="btn btn-mini pull-right tt">History</a>
				                    <a href="<%: Url.Action( "Viv", "Visualise", new { CorpusName = Model.CorpusName } ) %>" rel="tooltip" data-placement="top" title="<i>Eddy and Viv</i>: see and sort all translations of a segment, with back-translations" class="btn btn-mini pull-right tt">E &amp; V</a>
				                </div>
			                </div>
		                </div>
		            </div>
	            </div>
		            
	            <div class="row">
		            <div class="sorting span12">
				        <div class="pull-right subnav-item-right">
				            <div id="sort-direction" class="option-set" data-option-key="sortAscending">
				                <div class="btn-group pull-right">
				                	<a href="#sortAscending=true" data-option-value="true" class="btn btn-mini optionlink disabled">Asc</a>
				                    <a href="#sortAscending=false" data-option-value="false" class="btn btn-mini optionlink">Desc</a>
				                </div>
				                <div class="headline pull-right">ORDER</div>
				            </div>
				        </div>
				        <div class="pull-right subnav-item-right">
				            <div id="sort-by" class="option-set" data-option-key="sortBy">
				              <form class="form-horizontal viv-sort">
									<div class="control-label">SORT BY</div>
									<div class="controls">
										<select id="corpus-sort-select">
											<option data-option-value="year">Year</option>
											<option data-option-value="author">Author</option>
										</select>
									</div>
								</form>
				            </div>
				        </div>
				        <div>
				        	<div class="headline">&darr; <%: Model.VersionList.Length %> VERSIONS</div>
				        </div>
		            </div>
	            </div>
            </div>
        </div>
        <div id="corpus-index-content-wrapper" class="container">
	        <div id="corpus-index-content">
	            <%  if (Model.VersionList != null) foreach (var item in Model.VersionList)
	                    { %>
	            <div class="corpus-item version">
	            	<div class="admin pull-right">
	            		<a href="<%: Url.Action("Info", "Document", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion } ) %>" class="btn btn-mini tt" title="Learn more about this version" data-placement="left">Details</a>
	                    <% if (Model.CanWrite)
	                       { %>
	                    <a href="#" class="btn btn-mini btn-primary po-admin" rel="popover" title="Work">Work</a>
	                    <div class="admin-menu" style="display: none">
	                    	<div class="admin-buttons-wrapper">
			                    <%: Html.ActionLink("View", "Index", "Document", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
			                    <%: Html.ActionLink("Edit Details", "DocumentMetadata", "Corpus", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
			                    <%: Html.ActionLink("Upload", "Upload", "Document", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
			                    <%: Html.ActionLink("Align", "Index", "Aligner", new { CorpusName = Model.CorpusName, VersionName = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
			                    <a class="btn btn-mini" href="<%: Url.Action( "DeleteVersion", "Corpus", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion } ) %>"><i class="icon-trash"></i></a>
		                    </div>
	                    </div>
	                    <% } %>
                    </div>
                    
					<h3 class="version-label">
					<a href="<%: Url.Action( "Index", "Visualise", new {CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion} ) %>" class="version-title-link tt" title="Click to access Parallel View" data-placement="right"><%: item.NameIfVersion %> 
					<span class="year">(<%: item.ReferenceDate %>)</span> <small><%: item.Genre %>, <%: item.CopyrightInfo %></small>
					</a>
					</h3>
	            </div>
	            <% } %>
	        </div>
        </div>
    </div>
</asp:Content>