<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.CorpusModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	    VVV - Corpus:
    <%: Model.CorpusName%> - Admin
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
		<div class="page-header">
			<% if (Model.CanWrite)
               { %>
            <div class="corpus-options pull-right">
                <%: Html.ActionLink("Set up predefined segment attributes", "PredefinedSegmentAttributes", "Corpus", new { CorpusName = Model.CorpusName, NameIfVersion = string.Empty }, new { @class = "btn btn-mini" })%>
            </div>
            <%} %>
			<h1><%: Model.CorpusName%> - Admin</h1>
		</div>

        <div class="row">
        	<div class="span12">
	            Background calculation status: <span id="calcstatus"><%: ViewData["calcstatus"]%></span><br />
	            <input type="hidden" id="corpusbusy" value="<%: ViewData["corpusbusy"] %>" />       
	            <select id="variation-type-select">
	            </select>
	            <input type="button" id="startcalc" value="Start" />
	            <input type="button" id="stopcalc" value="Stop" />
			</div>
        </div>
    </div>
    <%: Html.HiddenFor(x => x.CorpusName, new { id = "corpusname" })%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-CorpusAdmin.js") %>" type="text/javascript"></script>

</asp:Content>
