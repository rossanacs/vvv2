﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EblaAPI;
using EblaImpl;
using System.Configuration;
using Prism.Models;
using System.Xml;

namespace Prism.Controllers
{
    [Authorize]
    public class DocumentController : PrismController
    {
        //==============================================
        //
        //  Page rendering
        //
        //==============================================

        public ActionResult Index(string CorpusName, string NameIfVersion)
        {
            
            try
            {
                ViewData["extract"] = string.Empty;
                ViewData["nextattribfilterrowindex"] = (0).ToString();
                ViewData["extractindented"] = string.Empty;
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
                EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = GetColouringData(atts);
            
                IDocument doc = PrismHelpers.GetDocument(CorpusName, NameIfVersion);

                ViewData["TOCSegments"]= ConstructContentList(atts, doc);

                DocumentExtractModel model = new DocumentExtractModel();
                //model.StartPos = 0;
                //model.Length = 500;
                //model.docModel = new DocumentModel();
                model.NameIfVersion = NameIfVersion;
                model.CorpusName = CorpusName;

                model.DocLength = doc.Length();
                DocumentMetadata meta = doc.GetMetadata();
                CorpusController.DocumentMetadataToModel(meta, model);

                return View(model);

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

		public ActionResult Info(string CorpusName, string NameIfVersion)
        {
            
            try
            {
                IDocument doc = PrismHelpers.GetDocument(CorpusName, NameIfVersion);
                DocumentMetadata meta = doc.GetMetadata();

				ViewData["CorpusName"] = CorpusName;
				ViewData["VersionName"] = NameIfVersion;
				ViewData["VersionDescription"]= meta.Description;
				ViewData["VersionLanguage"]= meta.LanguageCode;
				ViewData["VersionRefDate"]= meta.ReferenceDate;
				ViewData["VersionCopyrightInfo"]= meta.CopyrightInfo;
				ViewData["VersionGenre"]= meta.Genre;
				ViewData["VersionAuthor"]= meta.AuthorTranslator;
				ViewData["VersionInfo"]= meta.Information;

                return View();

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

      
        public ActionResult Upload(string CorpusName, string NameIfVersion)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
                HtmlUploadModel model = new HtmlUploadModel();
                model.CorpusName = CorpusName;
                model.NameIfVersion = NameIfVersion;
                model.encoding = PrismEncoding.UTF8;
                return View(model);

            }
            catch (Exception ex)
            {
                string message = string.Empty;
                message = "An error occurred while trying to open corpus '" + CorpusName + "': " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        [HttpPost]
        public ActionResult Upload(HtmlUploadModel model, HttpPostedFileBase file)
        {

            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    ModelState.AddModelError(string.Empty, "You must select a file.");
                    return View(model);

                }

                try
                {
                    byte[] data = new byte[file.ContentLength];

                    file.InputStream.Read(data, 0, file.ContentLength);

                    System.IO.MemoryStream ms = new System.IO.MemoryStream(data);

                    System.Text.Encoding encoding = PrismHelpers.PrismEncodingToTextEncoding(model.encoding);

                    System.IO.StreamReader sr = new System.IO.StreamReader(ms,encoding ); 

                    String html = sr.ReadToEnd();

                    HtmlErrors errors = null;
                    if (string.IsNullOrWhiteSpace(model.NameIfVersion))
                    {
                        errors = PrismHelpers.GetCorpus(model.CorpusName).UploadBaseText(html);
                    }
                    else
                    {
                        errors = PrismHelpers.GetCorpus(model.CorpusName).UploadVersion(model.NameIfVersion, html);
                    }

                    if (errors.HtmlParseErrors.Length > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Errors were encountered parsing the HTML file.");

                        foreach (var e in errors.HtmlParseErrors)
                        {
                            string message = e.Reason + " at line " + e.Line.ToString() + ", position " + e.LinePosition.ToString();
                            ModelState.AddModelError(string.Empty, message);

                            return View(model);

                        }

                        return View(model);

                    }


                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);

                    return View(model);
                }

            }
            else
            {
                return View(model);

            }
            return RedirectToAction("Index", "Document", new { CorpusName = model.CorpusName, NameIfVersion = model.NameIfVersion });


        }

        //==============================================
        //
        //  AJAX calls
        //
        //==============================================

        [HttpPost]
        public ActionResult GetBaseTextExtract(AlignModel model)
        {
            return GetExtract(model.BaseTextModel);
        }

        [HttpPost]
        public ActionResult GetVersionExtract(AlignModel model)
        {
            return GetExtract(model.VersionModel);
        }

        [HttpPost]
        public ActionResult GetBaseTextSegmentText(SimpleEddyChartModel model)
        {
            try
            {
                SegmentTextResultModel results = new SegmentTextResultModel();

                var basetext = PrismHelpers.GetBaseText(model.CorpusName);

                var seg = basetext.GetSegmentDefinition(model.BaseTextSegmentId);

                results.SegmentText = basetext.GetDocumentContentText(seg.StartPosition, seg.Length);
                results.Succeeded = true;

                return MonoWorkaroundJson(results);

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult GetEddyOverviewChartTooltipText(EddyOverviewChartTooltipModel model)
        {
            try
            {
                var results = new EddyOverviewChartTooltipResultModel();

                var basetext = PrismHelpers.GetBaseText(model.CorpusName);

                var seg = basetext.GetSegmentDefinition(model.BaseTextSegmentId);

                results.BaseTextSegmentText = basetext.GetDocumentContentText(seg.StartPosition, seg.Length);


                var set = PrismHelpers.GetAlignmentSet(model.CorpusName, model.VersionName);
                var a = set.FindAlignment(model.BaseTextSegmentId, true);
                if (a != null)
                {
                    var version = PrismHelpers.GetVersion(model.CorpusName, model.VersionName);
                    string s = string.Empty;
                    foreach (int segid in a.SegmentIDsInVersion)
                    {
                        seg = version.GetSegmentDefinition(segid);
                        s += version.GetDocumentContentText(seg.StartPosition, seg.Length) + " ";
                    }

                    results.VersionSegmentText = s.Trim();
                }

                results.Succeeded = true;

                return MonoWorkaroundJson(results);

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [ValidateInput(false), AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult SaveExtract(DocumentExtractModel model, string newHtml)
        {
            try
            {
                string htmlDoc = "<html><head></head><body>" + newHtml + "</body></html>";

                var corpus = PrismHelpers.GetCorpus(model.CorpusName);
                HtmlErrors errors = null;
                if (string.IsNullOrEmpty(model.NameIfVersion))
                {
                    errors = corpus.UpdateBaseText(htmlDoc);
                }
                else
                {
                    errors = corpus.UpdateVersion(model.NameIfVersion, htmlDoc);
                }

                if (errors.HtmlParseErrors.Length > 0)
                {
                    string s = "Errors were encountered parsing the HTML file." + Environment.NewLine;

                    foreach (var e in errors.HtmlParseErrors)
                    {
                        s += e.Reason + " at line " + e.Line.ToString() + ", position " + e.LinePosition.ToString() + Environment.NewLine;
                        
                    }

                    return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(s) });
                }

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        [HttpPost]
        public ActionResult GetExtract(DocumentExtractModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                int StartPosition = 0;
                int Length = 0;

                int segID = model.ExtractSegmentID;
                if (segID == -1)
                {
                    Length = doc.Length();
                }
                else
                {
                    if (model.Editing)
                        throw new Exception("Editing can currently only be performed if you are viewing the whole document, not just an extract.");
                    SegmentDefinition segdef = doc.GetSegmentDefinition(segID);
                    StartPosition = segdef.StartPosition;
                    Length = segdef.Length;
                }

                bool addSegmentFormatting = !model.Editing;

                var segfilter = EblaAttribFilterFromModel(model).ToArray();
                if (model.Editing)
                    segfilter = null; // we need all the segments emitted when editing

                string content = doc.GetDocumentContent(StartPosition, Length, true, addSegmentFormatting, segfilter, model.Editing);

                return MonoWorkaroundJson(new GetHtmlResultModel { Succeeded = true, html = content /*System.Net.WebUtility.HtmlEncode(content)*/ });


            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult ChangeSegmentMarker(ChangeSegmentMarkerModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                SegmentDefinition segDef = doc.GetSegmentDefinition(model.SegmentDefinitionID);

                if (segDef == null)
                    return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = "Segment definition with ID " + model.SegmentDefinitionID.ToString() + " not found." });

                if (model.start)
                {
                    if (model.newOffset > segDef.StartPosition + segDef.Length)
                        return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = "Start marker may not be placed after end marker." });

                    segDef.Length -= model.newOffset - segDef.StartPosition;
                    segDef.StartPosition = model.newOffset;
                }
                else
                {
                    if (model.newOffset < segDef.StartPosition)
                        return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = "End marker may not be placed before start marker." });

                    segDef.Length = model.newOffset - segDef.StartPosition;
                }

                doc.UpdateSegmentDefinition(segDef);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        [HttpPost]
        public ActionResult DeleteSegs(BaseDocumentModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                doc.DeleteAllSegmentDefinitions();

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult DeleteSeg(BaseDocumentModel model, int id)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                doc.DeleteSegmentDefinition(id);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpGet]
        public ActionResult AlignerBaseTextSegmentDefinitionDialog(AlignerBaseTextDocumentExtractModel model, int? id, int? startOffset, int? length)
        {
            return SegmentDefinitionDialog(model.BaseTextModel, id, startOffset, length);
        }

        [HttpGet]
        public ActionResult AlignerVersionSegmentDefinitionDialog(AlignerVersionDocumentExtractModel model, int? id, int? startOffset, int? length)
        {
            return SegmentDefinitionDialog(model.VersionModel, id, startOffset, length);
        }

        [HttpPost]
        public ActionResult AlignerBaseTextSegmentDefinitionDialog(SegmentDefinitionModel model)
        {
            return SegmentDefinitionDialog(model);
        }

        [HttpPost]
        public ActionResult AlignerVersionSegmentDefinitionDialog(SegmentDefinitionModel model)
        {
            return SegmentDefinitionDialog(model);
        }


        [HttpGet]
        public ActionResult SegmentDefinitionDialog( DocumentExtractModel model, int? id, int? startOffset, int? length)
        {
            int nextAttribRowIndex = 0;

            IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);
            SegmentDefinitionModel m = new SegmentDefinitionModel();
            m.CorpusName = model.CorpusName;
            m.NameIfVersion = model.NameIfVersion;


            ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);

            //m.PredefinedSegmentAttributes = new List<EblaAPI.PredefinedSegmentAttribute>( corpus.GetPredefinedSegmentAttributes());

            SegmentDefinition segdef = new SegmentDefinition();
            int maxLength = 40;
            int trimLength = maxLength / 2 - 2;
            if (id.HasValue)
            {
                segdef = doc.GetSegmentDefinition(id.Value);
            }
            else
            {
                if (!startOffset.HasValue || !length.HasValue)
                    throw new Exception("Either a non-null segment ID or valid startOffset/length must be provided.");

                // Were they viewing the whole text, or just an extract?
                if (model.ExtractSegmentID != -1)
                {
                    // An extract - so the actual start position of the new segment, with respect
                    // to the whole text, is the startoffset provided plus the extract segment start pos
                    SegmentDefinition extractSegment = doc.GetSegmentDefinition(model.ExtractSegmentID);
                    startOffset += extractSegment.StartPosition;
                }

                segdef.ID = -1;
                segdef.StartPosition = startOffset.Value;
                segdef.Length = length.Value;

                //// Add any attribs from the view filter

                //if (model.AttributeFilters != null)
                //{
                //    List<SegmentAttribute> segattribs = new List<SegmentAttribute>();
                //    foreach (var a in model.AttributeFilters)
                //    {
                //        SegmentAttribute attrib = new SegmentAttribute();
                //        attrib.Name = a.Name;
                //        attrib.Value = a.Value;
                //        segattribs.Add(attrib);
                //    }
                //    segdef.Attributes = segattribs.ToArray();
                //}

            }

            string content = doc.GetDocumentContentText(segdef.StartPosition, segdef.Length);
            m.Content = content;
            if (content.Length > maxLength)
            {
                string abbr = content.Substring(0, trimLength) + "[...]" + content.Substring(content.Length - trimLength);
                m.Content = abbr;
            }
            

            m.ID = segdef.ID;
            m.startPos = segdef.StartPosition;
            m.length = segdef.Length;

            List<AttributeModel> attribs = new List<AttributeModel>();

            if (segdef.Attributes != null)
                if (segdef.Attributes.Length > 0)
                    foreach (var a in segdef.Attributes)
                    {
                        AttributeModel attrib = new AttributeModel();
                        attrib.Name = a.Name;
                        attrib.Value = a.Value;
                        attribs.Add(attrib);
                        nextAttribRowIndex++;
                    }

            // Add any attribs from the view filter
            if (segdef.ID == -1)
                if (model.AttributeFilters != null)
                    attribs.AddRange(model.AttributeFilters);

            m.Attributes = attribs.ToArray();
            ViewData["nextattribrowindex"] = nextAttribRowIndex.ToString();

            return View("SegmentDefinitionDialog", m);
        }


        [HttpPost]
        public ActionResult SegmentDefinitionDialog(SegmentDefinitionModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                SegmentDefinition segDef = new SegmentDefinition();
                segDef.ID = model.ID;
                segDef.StartPosition = model.startPos;
                segDef.Length = model.length;
                List<SegmentAttribute> attribs = new List<SegmentAttribute>();
                if (model.Attributes != null)
                    foreach (var a in model.Attributes)
                    {
                        SegmentAttribute attrib = new SegmentAttribute();
                        attrib.Name = a.Name;
                        attrib.Value = a.Value;
                        attribs.Add(attrib);
                    }
                segDef.Attributes = attribs.ToArray();

                if (model.ID == -1)
                    doc.CreateSegmentDefinition(segDef);
                else
                    doc.UpdateSegmentDefinition(segDef);

                
                return JsonPopupResult(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return JsonPopupResult(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }

        }




        [HttpPost]
        public ActionResult AutoSegment2(BaseDocumentModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                AutoSegmenter.AutoSegment2(doc);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });


            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }

        }

        [HttpPost]
        public ActionResult AutoSegment1(BaseDocumentModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                AutoSegmenter.AutoSegment1(doc);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true});
                

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }

        }

        //==============================================
        //
        //  Implementation
        //
        //==============================================

        //private class ContentsEntry
        //{
        //    public int start;
        //    public int length;
        //    public string title;


        //}


        internal static SelectList ConstructContentList(EblaAPI.PredefinedSegmentAttribute[] atts, IDocument document)
        {
            List<string> attsToInclude = new List<string>();
            foreach (var a in atts)
            {
                if (a.ShowInTOC)
                    attsToInclude.Add(a.Name);
            }

            SegmentDefinition[] segs = document.FindSegmentDefinitions(0, document.Length(), false, false, null);

            //List<ContentsEntry> entries = new List<ContentsEntry>();

            Dictionary<int, string> TOCMap = new Dictionary<int, string>();
            List<SegmentDefinition> TOCsegs = new List<SegmentDefinition>();

            foreach (var seg in segs)
            {
                if (seg.Attributes != null)
                    foreach (var a in seg.Attributes)
                    {
                        if (attsToInclude.Contains(a.Name))
                        {
                            TOCMap.Add(seg.ID, a.Value);
                            TOCsegs.Add(seg);
                            /*ContentsEntry e = new ContentsEntry();
                            e.title = a.Value;
                            e.start = seg.StartPosition;
                            e.length = seg.Length;
                            entries.Add(e);*/
                            break;

                        }
                    }
            }

            List<SegmentTreeNode> nodeList = SegmentTreeNode.SegmentsToTree(TOCsegs, false);

            List<SelectListItem> TOCitems = new List<SelectListItem>();
            TOCitems.Add(new SelectListItem { Value = (-1).ToString(), Text = "[entire text]" });

            foreach (var n in nodeList)
            {
                CreateTOCItems(TOCitems, n, TOCMap, 0);
            }

            /*ViewData["TOCSegments"] =*/
            return new SelectList(TOCitems, "Value", "Text");

        }

        static void CreateTOCItems(List<SelectListItem> TOCitems, SegmentTreeNode n, Dictionary<int, string> TOCmap, int depth)
        {
            string item = new string('-', depth);

            item += TOCmap[n.SegmentDefinition.ID];

            TOCitems.Add(new SelectListItem { Value = n.SegmentDefinition.ID.ToString(), Text = item });

            foreach (SegmentTreeNode child in n.ChildSegmentDefinitions)
            {
                CreateTOCItems(TOCitems, child, TOCmap, depth + 1);
            }

        }

        
        static internal List<EblaAPI.SegmentAttribute> EblaAttribFilterFromModel(DocumentExtractModel model)
        {
            return EblaAttribsFromModel(model.AttributeFilters);

        }

        static internal List<EblaAPI.SegmentAttribute> EblaAttribsFromModel(AttributeModel[] attribs)
        {
            List<EblaAPI.SegmentAttribute> filterAttribs = new List<SegmentAttribute>();

            if (attribs != null)
                foreach (var a in attribs)
                {
                    SegmentAttribute attrib = new SegmentAttribute();
                    attrib.Name = a.Name;
                    attrib.Value = a.Value;
                    filterAttribs.Add(attrib);
                }

            return filterAttribs;
        }


    
    }
}
