﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EblaAPI;
using EblaImpl;
using System.Xml;

namespace Prism
{
    public class AutoSegmenter
    {
        private static void CreateSegDef(int startPosition, int length, List<SegmentAttribute> attribs, IDocument doc)
        {
            SegmentDefinition segdef = new SegmentDefinition();

            segdef.StartPosition = startPosition;
            segdef.Length = length;

            segdef.Attributes = attribs.ToArray();
            doc.CreateSegmentDefinition(segdef);

        }

        private static void AddSpeechSegDef(XmlNode speechStart, XmlNode speechEnd, string speaker, IDocument doc)
        {
            XmlDocPos startPos = new XmlDocPos();
            startPos.textContent = (XmlText)speechStart;
            startPos.offset = 0;
            XmlDocPos endPos = new XmlDocPos();
            endPos.textContent = (XmlText)speechEnd;
            endPos.offset = endPos.textContent.InnerText.Length;

            List<SegmentAttribute> sdattribs = SDAttribs();

            List<SegmentAttribute> attribs = new List<SegmentAttribute>();
            SegmentAttribute attrib = new SegmentAttribute();
            attrib.Name = "Type";
            attrib.Value = "Speech";
            attribs.Add(attrib);
            attrib = new SegmentAttribute();
            attrib.Name = "Speaker";
            attrib.Value = speaker;
            attribs.Add(attrib);

            // Check for inline S.D.
#if _VVV_PRESERVEWHITESPACE
            int startPosition = XmlDocPos.MeasureTo(startPos, true);
            int length = XmlDocPos.MeasureBetween(startPos, endPos, true);
#else
            int startPosition = XmlDocPos.MeasureTo(startPos, false, null);
            int length = XmlDocPos.MeasureBetween(startPos, endPos, false, null);
#endif
            string text = doc.GetDocumentContentText(startPosition, length);
            while (true)
            {
                if (text.Length == 0)
                    break;

                int i = text.IndexOf("[");
                int j = text.IndexOf("]");
                // text starts with the end of an S.D.?
                if (j > -1 && j < i)
                {
                    CreateSegDef(startPosition, j + 1, sdattribs, doc);
                    startPosition += j + 1;
                    text = text.Substring(j + 1);
                    continue;
                }


                // Text ends with an unterminated S.D.?
                if (i > -1 && j < 0)
                {
                    // Yes - any speech before it?
                    if (i > 0)
                        CreateSegDef(startPosition, i, attribs, doc);

                    CreateSegDef(startPosition + i, text.Length - i, sdattribs, doc);
                    break;

                }

                // Text has an S.D. in it?
                if (i > -1)
                {
                    // Yes - any speech before it?
                    if (i > 0)
                        CreateSegDef(startPosition, i, attribs, doc);

                    CreateSegDef(startPosition + i, j - i + 1, sdattribs, doc);
                    startPosition += j + 1;
                    text = text.Substring(j + 1);
                    continue;
                }

                CreateSegDef(startPosition, text.Length, attribs, doc);
                break;
            }

        }


        private static void AddSDSegDef(XmlNode segStart, XmlNode segEnd, IDocument doc)
        {
            XmlDocPos startPos = new XmlDocPos();
            startPos.textContent = (XmlText)segStart;
            startPos.offset = 0;
            XmlDocPos endPos = new XmlDocPos();
            endPos.textContent = (XmlText)segEnd;
            endPos.offset = endPos.textContent.InnerText.Length;


            SegmentDefinition segdef = new SegmentDefinition();

#if _VVV_PRESERVEWHITESPACE
            segdef.StartPosition = XmlDocPos.MeasureTo(startPos, true);

            segdef.Length = XmlDocPos.MeasureBetween(startPos, endPos, true);
#else
            segdef.StartPosition = XmlDocPos.MeasureTo(startPos, false, null);

            segdef.Length = XmlDocPos.MeasureBetween(startPos, endPos, false, null);
#endif

            //List<SegmentAttribute> attribs = new List<SegmentAttribute>();
            //SegmentAttribute attrib = new SegmentAttribute();
            //attrib.Name = "Type";
            //attrib.Value = "S.D.";
            //attribs.Add(attrib);

            segdef.Attributes = SDAttribs().ToArray();
            doc.CreateSegmentDefinition(segdef);
        }

        private static List<SegmentAttribute> SDAttribs()
        {
            List<SegmentAttribute> attribs = new List<SegmentAttribute>();
            SegmentAttribute attrib = new SegmentAttribute();
            attrib.Name = "Type";
            attrib.Value = "S.D.";
            attribs.Add(attrib);

            return attribs;
        }

        internal static void AutoSegment2(IDocument doc)
        {
            XmlDocument xmlDoc = PrismHelpers.XmlDocFromContent(doc.GetDocumentContent(0, doc.Length(), false, false, null, false));

            string speaker = string.Empty;
            bool collatingSpeech = false;
            bool collatingSD = false;

            List<XmlText> textNodes = new List<XmlText>();


            // This document type is made up of a series of lines enclosed in 'p' tags.
            foreach (XmlNode line in xmlDoc.DocumentElement.ChildNodes)
            {
                if (line.NodeType != XmlNodeType.Element)
                    continue;

                XmlElement lineElm = (XmlElement)line;

                if (string.Compare(lineElm.Name, "p") != 0)
                    continue;

                if (lineElm.ChildNodes.Count == 0)
                    continue;

                if (lineElm.ChildNodes[0].NodeType != XmlNodeType.Text)
                    continue;

                XmlText lineTextNode = (XmlText)(lineElm.ChildNodes[0]);

                string lineText = lineTextNode.InnerText;

                lineText = lineText.Trim();
                if (lineText.Length == 0)
                    continue;

                if (string.Compare(lineText.Substring(0, 1), "(") == 0)
                {
                    collatingSD = true;
                    if (collatingSpeech)
                    {
                        if (textNodes.Count > 0)
                        {
                            AddSpeechSegDef(textNodes[0], textNodes[textNodes.Count - 1], speaker, doc);
                            textNodes.Clear();
                        }

                    }
                    if (collatingSD)
                    {
                        if (textNodes.Count > 0)
                        {
                            AddSDSegDef(textNodes[0], textNodes[textNodes.Count - 1], doc);
                            textNodes.Clear();
                        }

                    }
                    collatingSpeech = false;
                    textNodes.Clear();
                }
                else if (string.Compare(lineText, lineText.ToUpper()) == 0)
                {
                    if (collatingSpeech)
                    {
                        if (textNodes.Count > 0)
                        {
                            AddSpeechSegDef(textNodes[0], textNodes[textNodes.Count - 1], speaker, doc);
                            textNodes.Clear();
                        }
                    }
                    collatingSpeech = true;
                    speaker = lineText;
                    textNodes.Clear();

                }
                else if (collatingSpeech)
                {
                    textNodes.Add(lineTextNode);

                }

                if (collatingSD)
                {
                    textNodes.Add(lineTextNode);
                    if (string.Compare(lineText.Substring(lineText.Length - 1, 1), ")") == 0)
                    {
                        collatingSD = false;
                        if (textNodes.Count > 0)
                        {
                            AddSDSegDef(textNodes[0], textNodes[textNodes.Count - 1], doc);
                            textNodes.Clear();

                        }
                    }
                }

            }

            if (collatingSD)
            {
                if (textNodes.Count > 0)
                {
                    AddSDSegDef(textNodes[0], textNodes[textNodes.Count - 1], doc);
                    textNodes.Clear();

                }

            }

            if (collatingSpeech)
            {
                if (textNodes.Count > 0)
                {
                    AddSpeechSegDef(textNodes[0], textNodes[textNodes.Count - 1], speaker, doc);
                    textNodes.Clear();
                }

            }

        }

        internal static void AutoSegment1(IDocument doc)
        {
            XmlDocument xmlDoc = PrismHelpers.XmlDocFromContent(doc.GetDocumentContent(0, doc.Length(), false, false, null, false));
//            XmlNodeList speechNodes = xmlDoc.SelectNodes("//span[@data-type='speech']");
            XmlNodeList speechNodes = xmlDoc.SelectNodes("//b");

            foreach (XmlNode speechNode in speechNodes)
            {
                if (speechNode.ChildNodes.Count == 1)
                {
                    XmlNode child = speechNode.ChildNodes[0];

                    string speaker = child.InnerText;

                    XmlNode speech = speechNode.NextSibling;

                    if (speech != null)
                    {
                        if (speech.ChildNodes.Count > 1)
                        {
                            //int childCount = 0;
                            XmlNode speechStart = null;
                            XmlNode speechEnd = null;
                            XmlElement e = null;

                            foreach (XmlNode speechChild in speech.ChildNodes)
                            {
                                switch (speechChild.NodeType)
                                {
                                    case XmlNodeType.Text:
                                        if (speechStart == null)
                                            speechStart = speechChild;
                                        speechEnd = speechChild;
                                        break;
                                    case XmlNodeType.Element:
                                        e = (XmlElement)speechChild;
                                        if (e.InnerText.Length > 0)
                                        {
                                            if (speechStart != null)
                                            {
                                                AddSpeechSegDef(speechStart, speechEnd, speaker, doc);
                                                speechStart = null;
                                                speechEnd = null;
                                            }
                                        }
                                        break;

                                }
                            }

                            if (speechStart != null)
                                AddSpeechSegDef(speechStart, speechEnd, speaker, doc);



                        }
                    }

                }
            }

            XmlNodeList sdNodes = xmlDoc.SelectNodes("//i");
            foreach (XmlNode sdNode in sdNodes)
            {
                if (sdNode.ChildNodes.Count == 1)
                {
                    if (sdNode.ChildNodes[0].NodeType == XmlNodeType.Text)
                    {
                        AddSDSegDef(sdNode.ChildNodes[0], sdNode.ChildNodes[0], doc);
                    }
                }
            }

            

        }

    }
}