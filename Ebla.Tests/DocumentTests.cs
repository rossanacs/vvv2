﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EblaAPI;
using HtmlAgilityPack;
using System.Xml;

namespace Ebla.Tests
{
    /// <summary>
    /// Summary description for UnitTest2
    /// </summary>
    [TestClass]
    public class DocumentTests
    {
        internal const int _testDocIndex = 6;
        internal string _testDocument = Helpers.TestDocs[_testDocIndex].Filename;
        internal System.Text.Encoding _encoding = Helpers.TestDocs[_testDocIndex].FileEncoding;

        public DocumentTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize() 
        {
            Helpers.CreateTestCorpus();
        }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion



        [TestMethod]
        public void RenderLength()
        {
            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), _testDocument);

            Helpers.UploadBaseTextToTestCorpus(path, _encoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

            int docLength = doc.Length();

            string content = doc.GetDocumentContent(0, docLength, false, false, null, false);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Helpers.DocContentToXmlDoc(content));

            int i = EblaImpl.XmlDocPos.Measure(xmlDoc.DocumentElement, false);

            Assert.AreEqual(docLength, i);

        }

        [TestMethod]
        public void BulkRenderLength()
        {
            for (int i = 0; i < Helpers.TestDocs.Count; i++)
            {
                System.Diagnostics.Debug.WriteLine("BulkRenderLength, doc " + i.ToString());
                _testDocument = Helpers.TestDocs[i].Filename;
                _encoding = Helpers.TestDocs[i].FileEncoding;
                RenderLength();
            }
        }
        
        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void FineGrainedExtract1()
        {
            ExtractTest(_testDocument, 1);

        }

        [TestMethod]
        public void BulkFineGrainedExtract1()
        {
            for (int i = 0; i < Helpers.TestDocs.Count; i++)
            {
                System.Diagnostics.Debug.WriteLine("BulkFineGrainedExtract1, doc " + i.ToString());
                _testDocument = Helpers.TestDocs[i].Filename;
                _encoding = Helpers.TestDocs[i].FileEncoding;
                FineGrainedExtract1();
            }
        }

        //[TestMethod]
        //[Timeout(1000 * 60 * 60 * 24)]
        //public void RenderTest()
        //{
        //    IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

        //    doc.GetDocumentContent(0, doc.Length(), true, true, null);
        //}


        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void FineGrainedFormat1()
        {
            FormatTest(_testDocument, 1, false);

        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void MediumGrainedFormat1()
        {
            FormatTest(_testDocument, 10, false);

        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void CoarseGrainedFormat1()
        {
            FormatTest(_testDocument, 100, false);

        }


        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void MediumGrainedHeadTailFormat1()
        {
            FormatTest(_testDocument, 50, true);

        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void BulkMediumGrainedHeadTailFormat1()
        {
            for (int i = 0; i < Helpers.TestDocs.Count; i++)
            {
                System.Diagnostics.Debug.WriteLine("BulkMediumGrainedHeadTailFormat1, doc " + i.ToString());
                _testDocument = Helpers.TestDocs[i].Filename;
                _encoding = Helpers.TestDocs[i].FileEncoding;
                FormatTest(_testDocument, 50, true);
            }
            

        }

        public void FormatTest(string filename, int granularity, bool headAndTailOnly)
        {
            int headAndTailLength = 100;


            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), filename);

            Helpers.UploadBaseTextToTestCorpus(path, _encoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

            int docLength = doc.Length();

            if (headAndTailOnly)
            {
                if (docLength < (headAndTailLength * 2 + 2))
                    headAndTailOnly = false;
            }

            SegmentDefinition segDef = new SegmentDefinition();

            bool startPosIsInTail = false;
            for (int startPos = 0; startPos < docLength; startPos += granularity)
            {
                if (headAndTailOnly)
                    if (!startPosIsInTail)
                        if (startPos > headAndTailLength)
                        {
                            startPos = docLength - headAndTailLength;
                            startPosIsInTail = true;
                        }
                    

                int lastLengthTrace = 0;
                //bool lengthIsInTail = false;
                for (int length = 0; length < docLength - startPos; length += granularity)
                {
                  /*  if (headAndTailOnly)
                        if (!lengthIsInTail)
                            if (length > headAndTailLength)
                            {
                                startPos = docLength - headAndTailLength;
                                startPosIsInTail = true;
                            }*/
                    try
                    {
                        if ((length - lastLengthTrace) > 500)
                        {
                            System.Diagnostics.Trace.WriteLine("FormatTest, startPos = " + startPos.ToString() + ", length = " + length.ToString());
                            lastLengthTrace = length;
                        }

                        doc.DeleteAllSegmentDefinitions();
                        segDef.StartPosition = startPos;
                        segDef.Length = length;

                        doc.CreateSegmentDefinition(segDef);

                        string content = doc.GetDocumentContent(0, docLength, false, true, null, false);

                        HtmlDocument htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(Helpers.DocContentToHtmlDoc(content));

                        if (htmlDoc.ParseErrors.Count() > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("There were errors parsing the document with startPos {0} and length {1}.\r\n", startPos, length);

                            foreach (var e in htmlDoc.ParseErrors)
                            {
                                sb.Append("Reason: " + e.Reason);
                                sb.Append(", SourceText: " + e.SourceText);

                                //error.SourceText = e.SourceText;
                                //error.Line = e.Line;
                                //error.Reason = e.Reason;
                                //error.StreamPosition = e.StreamPosition;
                                //error.LinePosition = e.LinePosition;

                                //parseErrors.Add(error);
                                sb.Append("\r\n");

                            }
                            throw new Exception(sb.ToString());

                        }

                        int totalChars = 0;

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.PreserveWhitespace = true;
                        xmlDoc.LoadXml(Helpers.DocContentToXmlDoc(content));

                        int i = EblaImpl.XmlDocPos.Measure(xmlDoc.DocumentElement, true);

                        Assert.AreEqual(docLength, i);



                        //                        CheckFormat(startPos, length, htmlDoc.DocumentNode, ref totalChars);
                        CheckFormat(startPos, length, xmlDoc.DocumentElement, ref totalChars);


                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.WriteLine("FormatTest1, exception thrown with startPos = " + startPos.ToString() + ", length = " + length.ToString());
                        throw e;
                    }

                }

            }


        }

        public void ExtractTest(string filename, int granularity)
        {
            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), filename);

            Helpers.UploadBaseTextToTestCorpus(path, _encoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

            int docLength = doc.Length();

            SegmentDefinition segDef = new SegmentDefinition();

            for (int startPos = 0; startPos < docLength; startPos += granularity)
            {
                int lastLengthTrace = 0;
                for (int length = 0; length < docLength - startPos; length += granularity)
                {
                    try
                    {
                        if ((length - lastLengthTrace) > 500)
                        {
                            System.Diagnostics.Trace.WriteLine("ExtractTest, startPos = " + startPos.ToString() + ", length = " + length.ToString());
                            lastLengthTrace = length;
                        }

                        string content = doc.GetDocumentContent(startPos, length, false, false, null, false);

                        HtmlDocument htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(Helpers.DocContentToHtmlDoc(content));

                        if (htmlDoc.ParseErrors.Count() > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("There were errors parsing the extract with startPos {0} and length {1}.\r\n", startPos, length);

                            foreach (var e in htmlDoc.ParseErrors)
                            {
                                sb.Append("Reason: " + e.Reason);
                                sb.Append(", SourceText: " + e.SourceText);

                                //error.SourceText = e.SourceText;
                                //error.Line = e.Line;
                                //error.Reason = e.Reason;
                                //error.StreamPosition = e.StreamPosition;
                                //error.LinePosition = e.LinePosition;

                                //parseErrors.Add(error);
                                sb.Append("\r\n");

                            }
                            throw new Exception(sb.ToString());

                        }


                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.WriteLine("FormatTest1, exception thrown with startPos = " + startPos.ToString() + ", length = " + length.ToString());
                        throw e;
                    }

                }

            }


        }
        

        void CheckFormat(int startPos, int length, XmlNode xmlNode, ref int totalChars)
        {

            foreach (XmlNode n in xmlNode.ChildNodes)
            {
                switch (n.NodeType)
                {
                        
                    case  XmlNodeType.Element:
                        if (true)
                        {
                            CheckFormat(startPos, length, n, ref totalChars);

                        }

                        break;
                    /*case XmlNodeType.Comment:
                        break;
                    */
                    case XmlNodeType.Whitespace:
                    case XmlNodeType.SignificantWhitespace:
                    case XmlNodeType.Text:
                        //if (string.IsNullOrWhiteSpace(n.InnerText))
                        //{
                        //    // Skip - otherwise serialisation makes it different
                        //}
                        //else
                        {

                            totalChars += n.InnerText.Length;


                            bool formatted = HasFormatting(n);
                            bool formatError = false;
                            if (totalChars < startPos + 1)
                            {
                                if (formatted)
                                    formatError = true;
                            }
                            else if (totalChars < startPos + 1 + length)
                            {
                                if (!formatted)
                                    formatError = true;

                            }
                            else
                            {
                                if (formatted)
                                    formatError = true;

                            }

                            if (formatError)
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.AppendFormat("Document formatting wrong at character {2} with startPos {0} and length {1}.", startPos, length, totalChars);
                                sb.Append("\r\n");
                                if (formatted)
                                    sb.Append("Content was formatted that should not have been.\r\n");
                                else
                                    sb.Append("Content was not formatted that should have been.\r\n");
                                sb.Append("Text node contents: " + n.InnerText + "\r\n");

                                throw new Exception(sb.ToString());
                            }

                        }
                        break;
                    default:
                        throw new Exception("Unexpected XmlNodeType: " + n.NodeType.ToString());
                }


            }


        }


        /*void CheckFormat(int startPos, int length, HtmlNode htmlNode, ref int totalChars)
        {

            foreach (var n in htmlNode.ChildNodes)
            {
                switch (n.NodeType)
                {
                    case HtmlNodeType.Element:
                        if (true)
                        {
                            CheckFormat(startPos, length, n, ref totalChars);

                        }

                        break;
                    case HtmlNodeType.Comment:
                        break;
                    case HtmlNodeType.Text:
                        //if (string.IsNullOrWhiteSpace(n.InnerText))
                        //{
                        //    // Skip - otherwise serialisation makes it different
                        //}
                        //else
                        {

                            totalChars += n.InnerText.Length;


                            bool formatted = HasFormatting(n);
                            bool formatError = false;
                            if (totalChars < startPos)
                            {
                                if (formatted)
                                    formatError = true;
                            }
                            else if (totalChars <= startPos + length)
                            {
                                if (!formatted)
                                    formatError = true;

                            }
                            else
                            {
                                if (formatted)
                                    formatError = true;

                            }

                            if (formatError)
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.AppendFormat("Document formatting wrong at character {2} with startPos {0} and length {1}.", startPos, length, totalChars);
                                sb.Append("\r\n");
                                if (formatted)
                                    sb.Append("Content was formatted that should not have been.\r\n");
                                else
                                    sb.Append("Content was not formatted that should have been.\r\n");
                                sb.Append("Text node contents: " + n.InnerText + "\r\n");

                                throw new Exception(sb.ToString());
                            }

                        }
                        break;
                    default:
                        throw new Exception("Unexpected HtmlNodeType: " + n.NodeType.ToString());
                }


            }


        }
        */
        //bool HasFormatting(HtmlNode n)
        //{
        //    if (n.NodeType == HtmlNodeType.Element)
        //        if (n.Attributes.Contains("class"))
        //            if (string.Compare(n.Attributes["class"].Value, "segformat") == 0)
        //                return true;

        //    if (n.ParentNode == null)
        //        return false;

        //    return HasFormatting(n.ParentNode);
        //}

        bool HasFormatting(XmlNode n)
        {
            if (n.NodeType == XmlNodeType.Element)
                if (n.Attributes["data-eblatype"] != null)
                    if (string.Compare(n.Attributes["data-eblatype"].Value, "segformat") == 0)
                        return true;

            if (n.ParentNode == null)
                return false;

            return HasFormatting(n.ParentNode);
        }



    }
}
