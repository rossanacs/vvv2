function createNav( id ) {

    var actBar = new Array(),
        actBarDesc = new Array(),
        actTextLength = new Array(),
        actBarTextWidth = 0,
        actBarWidth = 0,
        actBarScale = 0,
        sceneBar = new Array(),
        sceneBarDesc = new Array(),
        sceneBarWidth = 0,
        sceneBarScale = 0,
        navWidth = 350,
        lastActiveBar = 0,
        lastActiveScene = 2;

  // get structure from json id
    $.ajax({
      type: "GET",
      url: './input/text.json',
      success: function (data) {
        createTitle(id, data);
        createActBar(id, data);
        createSceneBar(id, data, 0);


      },
      error: function () {
        alert('can not load text');
      }
    });


    function createTitle(id, data) {
      // init layers
      layerTitle[ id ] = new Kinetic.Layer();

      var name = data.id[ id ].name;

       // init text object
      var title = new Kinetic.Text({
          text: "",
          fontFamily: "Georgia",
          fontSize: 13,
          padding: 0,
          textFill: "black",
          //textStrokeWidth: 3,
          //fill: "black",
          //alpha: 1,
          //connectorible: false,
          align: "left",
          verticalAlign: "top",
      });

          title.setText( name );
          title.setPosition( 0,0);
          //title.show();
          //layerSpeaker[ id ].draw();
          
          layerTitle[ id ].add( title );
          navStage[ id ].add(layerTitle[ id ]);
            
    }


    function createActBar(id, data) {
      
      // reset variables
      actBarTextWidth = 0;

      // init layers
      layerAct[ id ] = new Kinetic.Layer();
      layerActBarDesc[ id ] = new Kinetic.Layer();

      // get number of acts
      var actNumb = data.id[ id ].structure.act.length;

      // get text length for every act
      for (var i=0; i < actNumb; i++) {

          // init
          actTextLength[ i ] = 0;

          // calculate act size by getting all scene character length
          var sceneNumb = data.id[ id ].structure.act[ i ].scene.length;
          for(var j=0; j < sceneNumb; j++) {
            actTextLength[ i ] += data.id[ id ].structure.act[ i ].scene[ j ].textLength;
          }

          // calculate the text length of all bars
          actBarTextWidth += actTextLength[ i ]; 

      }
//console.log(actBarTextWidth);
      // add margin space
      actBarTextWidth = actBarTextWidth + ( actBarMargin * actNumb );

      // calculate scale size in dependency of the navWidth and total act bar textlength
      actBarScale = (actBarTextWidth / navWidth);


      // create rect for every act
      for (var i=0; i < actNumb; i++) {

          //change size in dependency of the scale
          var width = actTextLength[ i ] / actBarScale ;
          
          // init for +=
          actBar[ i ] = 0;
          actBarDesc[ i ] = 0;
          
          // draw rect 
          actBar[ i ] = new Kinetic.Rect({   
              x: actBarWidth,
              y: 0,
              width: width,
              height: actBarHeight,
              fill: actBarFill,
              name: data.id[ id ].structure.act[i].name,
              id: 'actBar_'+ i
              //$(speeches[i]).attr('id')
            });
          
             // init text object
            actBarDesc[ i ] = new Kinetic.Text({
                text: "",
                fontFamily: "Georgia",
                fontSize: 9,
                fontStyle: "normal",
                padding: 0,
                textFill: actBarDescFill,
                //fill: "black",
                alpha: 1,
                connectorible: false,
                align: "left",
                verticalAlign: "top",
            });

            // show description text 
            actBarDesc[ i ].setText( actBar[ i ].getName() );
            actBarDesc[ i ].setPosition( actBar[ i ].getPosition().x + barPaddingLeft, actBar[ i ].getPosition().y + barPaddingTop);


            // event listener
            actBar[ i ].on("mousemove", function() {
                document.body.style.cursor = "pointer";
                this.setFill(actBarFillHover);
                actBarDesc[ this.index ].setTextFill(actBarDescFillHover);
                layerAct[ id ].draw();
                layerActBarDesc[ id ].draw();  
            });
            
            actBar[ i ].on("mouseout", function() {
                document.body.style.cursor = "default";
                this.setFill(actBarFill);
                actBarDesc[ this.index  ].setTextFill(actBarDescFill);
                // the selected one should stay marked
                actBar[ lastActiveBar ].setFill(actBarFillSelected);
                actBarDesc[ lastActiveBar  ].setTextFill(actBarDescFillSelected);
                layerAct[ id ].draw();
                layerActBarDesc[ id ].draw(); 
            });

            actBar[ i ].on("click", function(e) {
              document.body.style.cursor = "pointer";
              e.preventDefault();

              actBar[ lastActiveBar ].setFill(actBarFill);
               actBarDesc[ lastActiveBar  ].setTextFill(actBarDescFill);
              lastActiveBar = this.index;
              this.setFill(actBarFillSelected);
              layerAct[ id ].draw();
              layerActBarDesc[ id ].draw();

              // cleare scene and draw new scene bar
              layerScene[ id ].clear();
              layerSceneBarDesc[ id ].clear();
              layerScene[ id ].removeChildren();
              layerSceneBarDesc[ id ].removeChildren();
              createSceneBar(id, data, this.index);
              layerScene[ id ].draw();
              layerSceneBarDesc[ id ].draw();  

            });



            // increase the width of the act bar
            actBarWidth += width + actBarMargin;

            //add act bar and desc to canvas
            layerAct[ id ].add( actBar[ i ] );
            layerActBarDesc[ id ].add( actBarDesc[ i ] );
            layerAct[ id ].setPosition( 0, titleTextHeight);
            layerActBarDesc[ id ].setPosition( 0, titleTextHeight);

        }

        //add layer with act bar to nav stage
        navStage[ id ].add(layerAct[ id ]); 
        navStage[ id ].add(layerActBarDesc[ id ]); 
        
        // mark active bar
        actBar[ lastActiveBar ].setFill(actBarFillSelected);
        actBarDesc[ lastActiveBar ].setTextFill(actBarDescFillSelected);
        layerAct[ id ].draw(); 
        layerActBarDesc[ id ].draw();  
    }



    function createSceneBar(id, data, activeAct) {

      // init layers
      layerScene[ id ] = new Kinetic.Layer();
      layerSceneBarDesc[ id ] = new Kinetic.Layer();

      // reset width
      sceneBarWidth = 0;

      // get number of scenes of the active Act
      var sceneNumb = data.id[ id ].structure.act[ activeAct ].scene.length;

      // calculate scale number in dependency of the total act character length and nav width
      var sceneBarScale =  ( actTextLength[ activeAct ] + (sceneBarMargin * sceneNumb )) / navWidth ; 

      // create rect for every scene
      for (var i=0; i < sceneNumb; i++) {

          // calculate scene size by getting scene character length
          var sceneTextLength = 0;
          sceneTextLength = data.id[ id ].structure.act[ activeAct ].scene[ i ].textLength;

          //change size in dependency of the scale
          var width = sceneTextLength / sceneBarScale * 1.04;

          // draw rect 
          sceneBar[ i ] = new Kinetic.Rect({   
            x: sceneBarWidth,
            y: actBarHeight + actSceneBarMargin + titleTextHeight,
            width: width,
            height: sceneBarHeight,
            fill: actBarFill,
            name: data.id[ id ].structure.act[ activeAct ].scene[ i ].name,
            id: 'sceneBar_'+ i
          });


           // init text object
          sceneBarDesc[ i ] = new Kinetic.Text({
              text: "",
              fontFamily: "Georgia",
              fontSize: 9,
              padding: 0,
              textFill: actBarDescFill,
              //fill: "black",
              alpha: 1,
              connectorible: false,
              align: "left",
              verticalAlign: "top",
          });

          // show description text 
          sceneBarDesc[ i ].setText( sceneBar[ i ].getName() );
          sceneBarDesc[ i ].setPosition( sceneBar[ i ].getPosition().x + barPaddingLeft, sceneBar[ i ].getPosition().y + barPaddingTop);

           // event listener
            sceneBar[ i ].on("mousemove", function() {
                document.body.style.cursor = "pointer";
                this.setFill(actBarFillHover);
                sceneBarDesc[ this.index ].setTextFill(actBarDescFillHover);
                layerScene[ id ].draw();
                layerSceneBarDesc[ id ].draw();  
            });
            
            sceneBar[ i ].on("mouseout", function() {
                document.body.style.cursor = "default";
                this.setFill(actBarFill);
                sceneBarDesc[ this.index  ].setTextFill(actBarDescFill);
                // the selected one should stay marked
                sceneBar[ lastActiveScene ].setFill(actBarFillSelected);
                sceneBarDesc[ lastActiveScene  ].setTextFill(actBarDescFillSelected);
                layerScene[ id ].draw();
                layerSceneBarDesc[ id ].draw(); 
            });

            sceneBar[ i ].on("click", function(e) {
              document.body.style.cursor = "pointer";
              e.preventDefault();

              sceneBar[ lastActiveScene ].setFill(actBarFill);
              sceneBarDesc[ lastActiveScene  ].setTextFill(actBarDescFill);
              lastActiveScene = this.index;
              this.setFill(actBarFillSelected);
              layerScene[ id ].draw();
              layerSceneBarDesc[ id ].draw();

            });


          // increase the width of the act bar
          sceneBarWidth += width + sceneBarMargin;
          
          //add  bar to canvas
          layerScene[ id ].add( sceneBar[ i ] );
          layerSceneBarDesc[ id ].add( sceneBarDesc[ i ] );

        }
        //add layer with act bar to nav stage
        navStage[ id ].add(layerScene[ id ]);
        navStage[ id ].add(layerSceneBarDesc[ id ]);

        // mark active scene
        sceneBar[ lastActiveScene ].setFill(actBarFillSelected);
        sceneBarDesc[ lastActiveScene ].setTextFill(actBarDescFillSelected);
        layerScene[ id ].draw(); 
        layerSceneBarDesc[ id ].draw(); 

    }
}


function showHideNav( id ) {

  var nav = $("#nav_canvas_" +id );
  var content = $("#content_" +id );

  if( nav.hasClass('visible') ) {
    //contentPositionTop =  0; 

    // hide nav
    nav.animate({ top: - contentPositionTop[ id ] + navHeightBt - 10}, 100, scrollStyle, function() { nav.removeClass('visible').addClass('hidden'); });
    // position up content
    content.animate({ top:  - contentPositionTop[ id ] + navHeightBt - 10}, 100, scrollStyle, function() { contentPositionTop[ id ] =  navHeightBt - 10;  if(filterStatus) updateConnector( id, 'nav'); });

  }
  else {
    //contentPositionTop += navHeight - 7*navTop; 

    // show nav
    nav.animate({ top: 0 }, 100, scrollStyle, function() { nav.removeClass('hidden').addClass('visible'); });
     // position down txt
    content.animate({ top:  0 }, 100, scrollStyle, function(){ contentPositionTop[ id ] = navHeight + navTop ; if(filterStatus) updateConnector( id, 'nav' ); });
  }
}

function showHideFilter( id ) {

  var t = $("#txt_" +id );
  var f = $('#filter_canvas_'+id);

    if( f.hasClass('visible') ) {
      // hide filter
      f.removeClass('visible').addClass('hidden');

      //move text to full size again
      t.animate({ left: 0, width:  t.width() + 150 }, 100, scrollStyle, function() {  if(filterStatus) updateConnector( id, 'nav' ); });
    }
    else {
      // move text to right
      t.animate({ left: 150, width:  t.width() - 150 }, 100, scrollStyle, function() {  
        //show filter
        f.removeClass('hidden').addClass('visible'); 
        
        if(filterStatus) updateConnector( id, 'nav' ); 
      });



    }

}





